/**
 * @file
 * Attaches behaviors for Menu Migration clipboard interactions.
 */

(function (Drupal) {

  /**
   * Copies the drush commands to clipboard in the import/export types listings.
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.menuMigrationClipboard = {
    attach(context) {

      const $elements = once('mm-clipboard-once', document.querySelectorAll('[id^="mm-copy-"]'));
      $elements.forEach((element) => {
        const targetId = element.id.replace('mm-copy', 'mm-command');
        element.onclick = function() {
          copyToClipboard(document.getElementById(targetId));
        }
      });

      function copyToClipboard(element) {
        const tempItem = document.createElement('input');

        tempItem.setAttribute('type','text');
        tempItem.setAttribute('display','none');

        let content = element;
        if (element instanceof HTMLElement) {
          content = element.innerHTML;
        }

        tempItem.setAttribute('value', content);
        document.body.appendChild(tempItem);

        tempItem.select();
        if (!navigator.clipboard) {
          document.execCommand('Copy');
        } else {
          if (!/(safari)/i.test(navigator.userAgent)) {
            navigator.permissions.query({ name: "clipboard-write" }).then((result) => {
              if (result.state === "granted" || result.state === "prompt") {
                writeToClipboard(tempItem.value, element);
              }
            });
          }
          else {
            writeToClipboard(tempItem.value, element);
          }
        }
        tempItem.parentElement.removeChild(tempItem);
      }

      function writeToClipboard(value, element) {
        navigator.clipboard.writeText(value).then(
          () => {
            writeStatus(Drupal.t('Copied!'), element, true);
          },
          () => {
            writeStatus(Drupal.t('Failed!'), element, false);
          },
        );
      }

      function writeStatus(status, element, success) {
        const targetId = element.id.replace('mm-command', 'mm-copy');
        const statusWrapper = document.createElement('span');
        statusWrapper.style.color = success ? 'green' : 'red';
        statusWrapper.style.fontStyle = 'italic';
        const statusText = document.createTextNode(Drupal.t(status));
        statusWrapper.appendChild(statusText);
        document.getElementById(targetId).after(statusWrapper);
        setTimeout(function (status) {
          status.parentNode.removeChild(status);
        }, 1000, statusWrapper);
      }

    }
  };
})(Drupal);
