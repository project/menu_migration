<?php

namespace Drupal\menu_migration;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Routing\AdminHtmlRouteProvider;
use Symfony\Component\Routing\Route;

/**
 * Provides HTML routes for export type & import type pages.
 */
class ImportExportRouteProvider extends AdminHtmlRouteProvider {

  /**
   * {@inheritdoc}
   */
  public function getRoutes(EntityTypeInterface $entity_type) {
    $collection = parent::getRoutes($entity_type);
    $entity_type_id = $entity_type->id();
    if ($exportFormRoute = $this->getExportFormRoute($entity_type)) {
      $collection->add("entity.{$entity_type_id}.export_form", $exportFormRoute);
    }
    if ($importFormRoute = $this->getImportFormRoute($entity_type)) {
      $collection->add("entity.{$entity_type_id}.import_form", $importFormRoute);
    }
    return $collection;
  }

  /**
   * Gets the export form route.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   The generated route, if available.
   */
  protected function getExportFormRoute(EntityTypeInterface $entity_type) {
    if ($entity_type->hasLinkTemplate('export-form')) {
      $entity_type_id = $entity_type->id();
      $route = new Route($entity_type->getLinkTemplate('export-form'));
      // Use the edit form handler, if available, otherwise default.
      $operation = 'default';
      if ($entity_type->getFormClass('export')) {
        $operation = 'export';
      }
      $route
        ->setDefaults([
          '_entity_form' => "{$entity_type_id}.{$operation}",
          '_title' => 'Export',
        ])
        ->setRequirement('_entity_access', "{$entity_type_id}.{$operation}")
        ->setOption('parameters', [
          $entity_type_id => ['type' => 'entity:' . $entity_type_id],
        ])
        ->setOption('_admin_route', TRUE);
      return $route;
    }
  }

  /**
   * Gets the export form route.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   The generated route, if available.
   */
  protected function getImportFormRoute(EntityTypeInterface $entity_type) {
    if ($entity_type->hasLinkTemplate('import-form')) {
      $entity_type_id = $entity_type->id();
      $route = new Route($entity_type->getLinkTemplate('import-form'));
      // Use the edit form handler, if available, otherwise default.
      $operation = 'default';
      if ($entity_type->getFormClass('import')) {
        $operation = 'import';
      }
      $route
        ->setDefaults([
          '_entity_form' => "{$entity_type_id}.{$operation}",
          '_title' => 'Import',
        ])
        ->setRequirement('_entity_access', "{$entity_type_id}.{$operation}")
        ->setOption('parameters', [
          $entity_type_id => ['type' => 'entity:' . $entity_type_id],
        ])
        ->setOption('_admin_route', TRUE);
      return $route;
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getCollectionRoute(EntityTypeInterface $entity_type) {
    if ($route = parent::getCollectionRoute($entity_type)) {
      // Add the global menu migration permission to the collection routes.
      $permissions = [
        $route->getRequirement('_permission'),
        'administer menu migration',
      ];
      $route->setRequirement('_permission', implode('+', $permissions));
      return $route;
    }
  }

}
