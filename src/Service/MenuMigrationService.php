<?php

namespace Drupal\menu_migration\Service;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Menu\MenuLinkTreeInterface;
use Drupal\Core\Menu\MenuTreeParameters;
use Drupal\menu_link_content\Entity\MenuLinkContent;
use Drupal\menu_link_content\Plugin\Menu\MenuLinkContent as MenuLinkContentPlugin;
use Drupal\menu_migration\Event\MenuImportEvent;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * Service for managing menu items migrations.
 */
class MenuMigrationService {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The menu link tree service.
   *
   * @var \Drupal\Core\Menu\MenuLinkTreeInterface
   */
  protected MenuLinkTreeInterface $menuTree;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Contracts\EventDispatcher\EventDispatcherInterface
   */
  protected EventDispatcherInterface $eventDispatcher;

  /**
   * The language manager service.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected LanguageManagerInterface $languageManager;

  /**
   * Menu migration logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected LoggerChannelInterface $loggerChannel;

  public function __construct(EntityTypeManagerInterface $entityTypeManager, MenuLinkTreeInterface $menuLinkTree, EventDispatcherInterface $eventDispatcher, LanguageManagerInterface $languageManager, LoggerChannelFactoryInterface $logger) {
    $this->entityTypeManager = $entityTypeManager;
    $this->menuTree = $menuLinkTree;
    $this->eventDispatcher = $eventDispatcher;
    $this->languageManager = $languageManager;
    $this->loggerChannel = $logger->get('menu_migration');
  }

  /**
   * Gets the available menus.
   *
   * @return array
   *   Returns the available menus as associative array, where the key is the ID
   *   and the value is the label.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getAvailableMenus() {
    $menus = [];
    $availableMenus = $this->entityTypeManager->getStorage('menu')->loadMultiple();
    foreach ($availableMenus as $menu) {
      $menus[$menu->id()] = $menu->label();
    }
    return $menus;
  }

  /**
   * Deletes existing menu items for a given menu.
   *
   * @param string $menuName
   *   The machine name of the menu.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function deleteExistingMenuItems(string $menuName) {
    $existingItems = $this->entityTypeManager->getStorage('menu_link_content')->loadByProperties(['menu_name' => $menuName]);
    foreach ($existingItems as $existingItem) {
      $existingItem->delete();
    }
  }

  /**
   * Generates menu items and stores them in the database.
   *
   * @param array $sourceItems
   *   The hierarchy of menu items with Menu link content properties.
   * @param string $menuName
   *   The menu name that we are generating items for.
   * @param int $parent
   *   The menu link parent. Defaults to 0.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function generateMenuItems(array $sourceItems, string $menuName, $parent = 0) {
    foreach ($sourceItems as $item) {
      // Allow others to alter an item right before save. Does not include
      // translations.
      $menuItemEvent = new MenuImportEvent($item);
      $this->eventDispatcher->dispatch($menuItemEvent, MenuImportEvent::IMPORT_MENU_ITEM);
      $item = $menuItemEvent->getMenuItem();
      $item['parent'] = $parent;
      // Always override the menu name, as this can now be controlled in the
      // config entities.
      $item['menu_name'] = $menuName;
      $translations = $item['translations'] ?? [];
      unset($item['translations']);
      $menuLinkContent = MenuLinkContent::create($item);
      $menuLinkContent->save();
      foreach ($translations as $langcode => $translation) {
        $language = $this->languageManager->getLanguage($langcode);
        if (!empty($language)) {
          $menuLinkContent->addTranslation($langcode, $translation)->save();
        }
        else {
          // If you import a menu with translations from a multilingual site to
          // a non-multilingual one the above line will crash the execution.
          // @todo For now, we log a warning without message, but need to think of a better way.
          $this->loggerChannel->warning('Failed to add %language translation for %item_label because the target language does not exist.', [
            '%language' => $langcode,
            '%item_label' => $menuLinkContent->label(),
          ]);
        }
      }
      if (!empty($item['children'])) {
        $this->generateMenuItems($item['children'], $menuName, "menu_link_content:{$menuLinkContent->uuid()}");
      }
    }
  }

  /**
   * Gets the menu tree for the given menu name.
   *
   * @param string $menuName
   *   The menu name.
   *
   * @return array
   *   Returns an array with the menu hierarchical structure.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getMenuTree($menuName) {
    $parameters = new MenuTreeParameters();
    $tree = $this->menuTree->load($menuName, $parameters);
    $manipulators = [
      ['callable' => 'menu.default_tree_manipulators:generateIndexAndSort'],
    ];
    $tree = $this->menuTree->transform($tree, $manipulators);
    $processedTree = [];
    $this->processTree($tree, $processedTree);
    return $processedTree;
  }

  /**
   * Processes the given menu tree.
   *
   * Converts the given menu tree in a tree of hierarchical items, where each
   * item is a set of menu link properties.
   *
   * @param \Drupal\Core\Menu\MenuLinkTreeElement[] $tree
   *   The generated menu tree.
   * @param array $processedTree
   *   The resulted array.
   * @param int $delta
   *   The delta used for building the hierarchy.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function processTree(array &$tree, array &$processedTree, $delta = 0) {
    foreach ($tree as &$element) {
      $definition = $element->link->getPluginDefinition();
      if ($element->link instanceof MenuLinkContentPlugin) {
        // Unfortunately, we don't have access to the menu link content which
        // exists in the menu link content plugin, but it's protected.
        /** @var \Drupal\menu_link_content\Entity\MenuLinkContent $menuLinkContent */
        $menuLinkContent = $this->entityTypeManager->getStorage('menu_link_content')->load($definition['metadata']['entity_id']);
        $translations = [];
        $translationLanguages = $menuLinkContent->getTranslationLanguages(FALSE);
        foreach ($translationLanguages as $translationLanguage) {
          $langcode = $translationLanguage->getId();
          if ($menuLinkContent->hasTranslation($langcode)) {
            $menuItemTranslation = $menuLinkContent->getTranslation($langcode);
            foreach ($this->getTranslatableFields($menuLinkContent) as $translatableField) {
              $translations[$langcode][$translatableField] = $menuItemTranslation->get($translatableField)->getValue();
            }
          }
        }
        $processedTree[$delta] = [
          'url' => $definition['url'],
          // @todo We kinda need to load the tree in the default language.
          // Workaround: the plugin definition comes in non-default language,
          // need to investigate that.
          'title' => $menuLinkContent->get('title')->getString(),
          'description' => $menuLinkContent->get('description')->getString(),
          'options' => $definition['options'],
          'enabled' => $definition['enabled'],
          'expanded' => $definition['expanded'],
          'weight' => $definition['weight'],
          'menu_name' => $definition['menu_name'],
          'link' => $menuLinkContent->get('link')->getValue()[0],
          'translations' => $translations,
        ];
        $this->attachExtraFields($processedTree[$delta], $menuLinkContent);
        if ($element->subtree) {
          $processedTree[$delta]['children'] = [];
          $this->processTree($element->subtree, $processedTree[$delta]['children']);
        }
        $delta++;
      }
    }
  }

  /**
   * Gets the menu link content translatable field names.
   *
   * @param \Drupal\menu_link_content\Entity\MenuLinkContent $menuLinkContent
   *   A menu link content entity.
   *
   * @return int[]|string[]
   *   A list of translatable fields with some filtered out.
   */
  protected function getTranslatableFields(MenuLinkContent $menuLinkContent) {
    // @todo For now we're skipping these fields that are marked as translatable. Will deal with them later.
    $skippedFields = [
      'langcode',
      'changed',
      'default_langcode',
      'revision_translation_affected',
      'metatag',
      'content_translation_source',
      'content_translation_outdated',
      'content_translation_uid',
      'content_translation_status',
      'content_translation_created',
    ];
    return array_diff(array_keys($menuLinkContent->getTranslatableFields()), $skippedFields);
  }

  /**
   * Attaches non-excluded menu link content field values.
   *
   * @param array $menuItem
   *   The current menu item array prepared for export.
   * @param \Drupal\menu_link_content\Entity\MenuLinkContent $menuLinkContent
   *   The current MenuLinkContent entity.
   */
  protected function attachExtraFields(array &$menuItem, MenuLinkContent $menuLinkContent) {
    // In order to include any new fields belonging to contrib modules or custom
    // modules, but also preserve the existing behaviour, we're creating a list
    // of skipped fields. This can be adjusted/changed in the future.
    // @todo Revise this.
    $skippedFields = [
      'id',
      'uuid',
      'revision_id',
      'langcode',
      'bundle',
      'revision_created',
      'revision_user',
      'revision_log_message',
      'external',
      'rediscover',
      'parent',
      'changed',
      'default_langcode',
      'revision_default',
      'revision_translation_affected',
    ];
    $fieldDefinitions = $menuLinkContent->getFields(FALSE);
    $fields = array_diff_key($fieldDefinitions, $menuItem, array_combine($skippedFields, $skippedFields));
    foreach ($fields as $fieldName => $field) {
      $menuItem[$fieldName] = $field->getValue();
    }
  }

}
