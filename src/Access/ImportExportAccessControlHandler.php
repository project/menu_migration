<?php

namespace Drupal\menu_migration\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Provides an entity access control handler for export types & import types.
 */
class ImportExportAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    if ($account->hasPermission('administer menu migration')
      || $account->hasPermission($this->entityType->getAdminPermission())) {
      return AccessResult::allowed()->cachePerPermissions();
    }
    if ($operation == 'export') {
      return AccessResult::allowedIfHasPermission($account, 'perform export on menu migrations');
    }
    if ($operation == 'import') {
      return AccessResult::allowedIfHasPermission($account, 'perform import on menu migrations');
    }
    return parent::checkAccess($entity, $operation, $account);
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    $access = parent::checkCreateAccess($account, $context, $entity_bundle);
    return $access->orIf(AccessResult::allowedIfHasPermission($account, 'administer menu migration'));
  }

}
