<?php

namespace Drupal\menu_migration\Plugin;

/**
 * Interface for the Import Source plugins.
 */
interface ImportSourceInterface extends ImportExportPluginInterface {

  /**
   * Imports one or more menu hierarchies.
   *
   * The structure of the response should match the example below. The keys are
   * not mandatory, meaning that if all menus succeeded, the failure key
   * doesn't need to be set.
   *
   * @code
   * $response = [
   *   'success' => $success_menus,
   *   'failure' => $failed_menus,
   *   'errors' => $additional_error_messages,
   * ];
   * @endcode
   *
   * @return array
   *   Returns an associative array with the menus that succeeded and the ones
   *   that failed. It can also contain additional error messages in the
   *   "errors" key for the menus that failed.
   */
  public function import();

  /**
   * Manages the import of a single menu hierarchy.
   *
   * @param string $menuName
   *   The menu name (machine name).
   *
   * @return bool
   *   Returns TRUE if the import was successful, FALSE otherwise.
   *
   * @throws \Drupal\menu_migration\MenuMigrationException
   */
  public function importMenu(string $menuName);

  /**
   * Gets the detailed import description used prior the import execution.
   *
   * This description is used in the Import confirmation form, and in Drush for
   * plugins that support CLI to offer more details about the import source
   * location or any other relevant information.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup[]|string[]
   *   An array of translatable strings containing the description. If it
   *   contains HTML, the tags will be stripped when used with Drush.
   */
  public function getImportDescription();

}
