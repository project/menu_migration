<?php

namespace Drupal\menu_migration\Plugin;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\menu_migration\Service\MenuMigrationService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a base implementation for ExportDestination and ImportSource plugin.
 *
 * @see \Drupal\menu_migration\Annotation\MenuMigrationDestination
 * @see \Drupal\menu_migration\Annotation\MenuMigrationSource
 * @see \Drupal\menu_migration\Plugin\ExportDestinationManager
 * @see \Drupal\menu_migration\Plugin\ImportSourceManager
 * @see \Drupal\menu_migration\Plugin\ExportDestinationInterface
 * @see \Drupal\menu_migration\Plugin\ImportSourceInterface
 * @see plugin_api
 */
class ImportExportPluginBase extends PluginBase implements ContainerFactoryPluginInterface, ImportExportPluginInterface {

  /**
   * The format plugin manager.
   *
   * @var \Drupal\menu_migration\Plugin\FormatManager
   */
  protected FormatManager $formatManager;

  /**
   * The menu migration service.
   *
   * @var \Drupal\menu_migration\Service\MenuMigrationService
   */
  protected MenuMigrationService $menuMigrationService;

  /**
   * The format plugin.
   *
   * @var \Drupal\menu_migration\Plugin\FormatInterface
   */
  protected FormatInterface $formatPlugin;

  public function __construct(array $configuration, $plugin_id, $plugin_definition, FormatManager $formatManager, MenuMigrationService $menuMigrationService) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->setConfiguration($configuration);
    $this->formatManager = $formatManager;
    $this->menuMigrationService = $menuMigrationService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.menu_migration_format'),
      $container->get('menu_migration.import_export'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'format' => '',
      'menus' => [],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    $this->configuration = $configuration + $this->defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    return $this->pluginDefinition['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function getMenus() {
    // Ensure that "menus" is always processed as array. When using radios, the
    // value will be string.
    return array_filter((array) $this->configuration['menus'], function ($item) {
      return !empty($item);
    });
  }

  /**
   * {@inheritdoc}
   */
  public function getFormat() {
    return $this->configuration['format'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormatPlugin() {
    if (!isset($this->formatPlugin)) {
      $config['#object'] = $this;
      // @todo Handle errors and exceptions.
      $this->formatPlugin = $this->formatManager->createInstance($this->getFormat(), $config);
    }
    return $this->formatPlugin;
  }

  /**
   * {@inheritdoc}
   */
  public function hasValidFormat() {
    $formatDefinition = $this->formatManager->getDefinition($this->getFormat(), FALSE);
    return !empty($formatDefinition);
  }

  /**
   * {@inheritdoc}
   */
  public function handleMultiple() {
    return (bool) $this->pluginDefinition['multiple'];
  }

  /**
   * {@inheritdoc}
   */
  public function handleCli() {
    return (bool) $this->pluginDefinition['cli'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $this->buildFormatElement($form, $form_state);
    $this->buildMenusElement($form, $form_state);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {}

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {}

  /**
   * {@inheritdoc}
   */
  public function configurationSummary() {
    return [];
  }

  /**
   * Builds the Format form element for source and destination plugin forms.
   *
   * @param array $form
   *   An associative array containing the initial structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the complete form.
   */
  protected function buildFormatElement(array &$form, FormStateInterface $form_state) {
    $formats = $this->formatManager->getFormats();
    $form['format'] = [
      '#type' => 'select',
      '#title' => $this->t('Format'),
      '#description' => $this->t('Choose the format for the exported data.'),
      '#options' => $formats,
      '#required' => TRUE,
      '#empty_value' => '',
      '#default_value' => $this->getFormat(),
    ];
  }

  /**
   * Builds the Menus form element for source and destination plugin forms.
   *
   * @param array $form
   *   An associative array containing the initial structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the complete form.
   */
  protected function buildMenusElement(array &$form, FormStateInterface $form_state) {
    $count = $this->handleMultiple() ? 2 : 1;
    $form['menus'] = [
      '#type' => $this->handleMultiple() ? 'checkboxes' : 'radios',
      '#title' => $this->t('Menus'),
      '#description' => $this->formatPlural(
        $count,
        'Select 1 menu that you wish to export.',
        'Select 1 or more menus that you wish to export.'
      ),
      '#options' => $this->menuMigrationService->getAvailableMenus(),
      '#required' => TRUE,
      '#default_value' => $this->configuration['menus'],
    ];
  }

}
