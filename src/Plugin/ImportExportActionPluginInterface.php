<?php

namespace Drupal\menu_migration\Plugin;

use Drupal\Core\Form\FormStateInterface;

/**
 * Interface for export/import confirmation forms.
 *
 * This is used as an addition to the import & export types entity forms,
 * allowing source and destination plugins to interfere with import/export
 * confirmation forms.
 */
interface ImportExportActionPluginInterface extends ImportExportPluginInterface {

  /**
   * Builds the plugin Action form for source and destination plugin forms.
   *
   * @param array $form
   *   An associative array containing the initial structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the complete form.
   */
  public function buildActionForm(array $form, FormStateInterface $form_state);

  /**
   * Handles validation for source/destination action forms.
   *
   * @param array $form
   *   An associative array containing the initial structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the complete form.
   */
  public function validateActionForm(array $form, FormStateInterface $form_state);

  /**
   * Handles submission for source/destination action forms.
   *
   * @param array $form
   *   An associative array containing the initial structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the complete form.
   */
  public function submitActionForm(array $form, FormStateInterface $form_state);

}
