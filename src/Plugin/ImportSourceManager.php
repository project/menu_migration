<?php

namespace Drupal\menu_migration\Plugin;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\menu_migration\Attribute\MenuMigrationSource;

/**
 * Provides an Import Source plugin manager.
 *
 * @see \Drupal\menu_migration\Attribute\MenuMigrationSource
 * @see \Drupal\menu_migration\Annotation\MenuMigrationSource
 * @see \Drupal\menu_migration\Plugin\ImportSourceInterface
 * @see \Drupal\menu_migration\Plugin\menu_migration\ImportSource\ImportSourceBase
 * @see plugin_api
 */
class ImportSourceManager extends DefaultPluginManager {

  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/menu_migration/ImportSource',
      $namespaces,
      $module_handler,
      'Drupal\menu_migration\Plugin\ImportSourceInterface',
      MenuMigrationSource::class,
      'Drupal\menu_migration\Annotation\MenuMigrationSource',
    );
    $this->alterInfo('menu_migration_source_info');
    $this->setCacheBackend($cache_backend, 'menu_migration_source_info');
  }

  /**
   * Gets the available import sources as associative array.
   *
   * @return array
   *   Returns an associative array of the import sources keyed by their ID,
   *   having their label as the value.
   */
  public function getImportSources() {
    return array_map(function ($definition) {
      return $definition['label'];
    }, $this->getDefinitions());
  }

}
