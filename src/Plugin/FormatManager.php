<?php

namespace Drupal\menu_migration\Plugin;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\menu_migration\Attribute\MenuMigrationFormat;

/**
 * Provides a Format plugin manager.
 *
 * @see \Drupal\menu_migration\Attribute\MenuMigrationFormat
 * @see \Drupal\menu_migration\Annotation\MenuMigrationFormat
 * @see \Drupal\menu_migration\Plugin\FormatInterface
 * @see \Drupal\menu_migration\Plugin\menu_migration\Format\FormatBase
 * @see plugin_api
 */
class FormatManager extends DefaultPluginManager {

  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/menu_migration/Format',
      $namespaces,
      $module_handler,
      'Drupal\menu_migration\Plugin\FormatInterface',
      MenuMigrationFormat::class,
      'Drupal\menu_migration\Annotation\MenuMigrationFormat',
    );
    $this->alterInfo('menu_migration_format_info');
    $this->setCacheBackend($cache_backend, 'menu_migration_format_info');
  }

  /**
   * Gets the available formats as associative array.
   *
   * @return array
   *   Returns an associative array of the formats keyed by their ID, having
   *   their label as the value.
   */
  public function getFormats() {
    return array_map(function ($definition) {
      return $definition['label'];
    }, $this->getDefinitions());
  }

}
