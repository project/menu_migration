<?php

namespace Drupal\menu_migration\Plugin;

/**
 * Interface for the Format plugins.
 */
interface FormatInterface {

  /**
   * Gets the label of the Plugin.
   *
   * @return string
   *   The plugin label.
   */
  public function label();

  /**
   * Encodes the given menu hierarchy.
   *
   * This process is done prior to export operations.
   *
   * @param array $menuTree
   *   An array containing the menu tree ready for export.
   *
   * @return mixed
   *   Returns the encoded menu tree for export.
   */
  public function encode(array $menuTree);

  /**
   * Decodes the given menu hierarchy prior to import.
   *
   * @param mixed $menuTree
   *   The encoded menu tree.
   *
   * @return array
   *   Returns an array of the menu hierarchy ready for import.
   */
  public function decode(mixed $menuTree);

  /**
   * Gets the allowed extensions for the current format.
   *
   * This currently applies very well with the available plugins, but I don't
   * know if in the future it might conflict with new types or formats or not.
   *
   * @return array
   *   Returns an array with the allowed file extensions.
   */
  public function allowedExtensions();

  /**
   * Gets the default file extension for the current format.
   *
   * @return string
   *   Returns a file extension that will be used in some of the export plugins.
   */
  public function defaultExtension();

  /**
   * Gets the mime type of the file for downloadable exports.
   *
   * @return string
   *   A string with the file mime type.
   */
  public function mimeType();

}
