<?php

namespace Drupal\menu_migration\Plugin\menu_migration\Format;

use Drupal\Component\Serialization\Yaml;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\menu_migration\Attribute\MenuMigrationFormat;

/**
 * Provides the YAML import/export Format.
 */
#[MenuMigrationFormat(
  id: 'yaml',
  label: new TranslatableMarkup('YAML')
)]
class YamlFormat extends FormatBase {

  /**
   * {@inheritdoc}
   */
  public function encode(array $menuTree) {
    return Yaml::encode($menuTree);
  }

  /**
   * {@inheritdoc}
   */
  public function decode(mixed $menuTree) {
    return Yaml::decode($menuTree);
  }

  /**
   * {@inheritdoc}
   */
  public function allowedExtensions() {
    return ['yaml', 'yml'];
  }

  /**
   * {@inheritdoc}
   */
  public function defaultExtension() {
    return 'yaml';
  }

  /**
   * {@inheritdoc}
   */
  public function mimeType() {
    // @todo Not sure what the good mimetype is, using text for now.
    return 'text/plain';
  }

}
