<?php

namespace Drupal\menu_migration\Plugin\menu_migration\Format;

use Drupal\Core\Plugin\PluginBase;
use Drupal\menu_migration\Plugin\FormatInterface;

/**
 * Provides a base implementation for a Format plugin.
 *
 * @see \Drupal\menu_migration\Annotation\MenuMigrationFormat
 * @see \Drupal\menu_migration\Plugin\FormatManager
 * @see \Drupal\menu_migration\Plugin\FormatInterface
 * @see plugin_api
 */
abstract class FormatBase extends PluginBase implements FormatInterface {

  /**
   * {@inheritdoc}
   */
  public function label() {
    return $this->pluginDefinition['label'];
  }

}
