<?php

namespace Drupal\menu_migration\Plugin\menu_migration\Format;

use Drupal\Component\Serialization\Json;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\menu_migration\Attribute\MenuMigrationFormat;

/**
 * Provides the JSON import/export Format.
 */
#[MenuMigrationFormat(
  id: 'json',
  label: new TranslatableMarkup('JSON')
)]
class JsonFormat extends FormatBase {

  /**
   * {@inheritdoc}
   */
  public function encode(array $menuTree) {
    return Json::encode($menuTree);
  }

  /**
   * {@inheritdoc}
   */
  public function decode(mixed $menuTree) {
    return Json::decode($menuTree);
  }

  /**
   * {@inheritdoc}
   */
  public function allowedExtensions() {
    return ['json'];
  }

  /**
   * {@inheritdoc}
   */
  public function defaultExtension() {
    return 'json';
  }

  /**
   * {@inheritdoc}
   */
  public function mimeType() {
    return 'application/json';
  }

}
