<?php

namespace Drupal\menu_migration\Plugin\menu_migration\ImportSource;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\menu_migration\Attribute\MenuMigrationSource;
use Drupal\menu_migration\MenuMigrationException;

/**
 * Provides a codebase import destination.
 */
#[MenuMigrationSource(
  id: 'codebase',
  label: new TranslatableMarkup('Codebase'),
  cli: TRUE
)]
class Codebase extends ImportSourceBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'import_path' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['import_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Import directory path'),
      '#description' => $this->t("Enter the path from where the items should be imported from (relative to Drupal root - %root_path), no trailing slash. <br>Your menu(s) will be retrieved from this exact location. <br>E.g. If the defined path is %import_path, the selected menus will be imported from %import_location.", [
        '%root_path' => DRUPAL_ROOT,
        '%import_path' => !empty($this->configuration['import_path']) ? $this->configuration['import_path'] : '../config/menu_migration/DIRECTORY',
        '%import_location' => !empty($this->configuration['import_path']) ? $this->getSourceDirectory() : $this->getSourceDirectory() . '../config/menu_migration/DIRECTORY',
      ]),
      '#default_value' => $this->configuration['import_path'],
      '#attributes' => ['placeholder' => '../config/menu_migration/DIRECTORY'],
      '#required' => TRUE,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);
    // Ensure that there are no trailing slashes, nor white spaces.
    $form_state->setValue('import_path', trim(rtrim($form_state->getValue('import_path'), '/')));
  }

  /**
   * {@inheritdoc}
   */
  public function importMenu(string $menuName) {
    $format = $this->getFormatPlugin();
    $directory = $this->getSourceDirectory();
    $extension = $format->defaultExtension();
    $file = "{$directory}/{$menuName}.{$extension}";
    $contents = @file_get_contents($file);
    if ($contents !== FALSE) {
      $items = $format->decode($contents);
      $this->menuMigrationService->generateMenuItems($items, $menuName);
      return TRUE;
    }
    throw new MenuMigrationException(sprintf('The "%s" file does not exist or cannot be opened.', $file));
  }

  /**
   * {@inheritdoc}
   */
  public function getImportDescription() {
    $description = parent::getImportDescription();
    $menus = $this->getMenus();
    $path = $this->configuration['import_path'];
    $extension = $this->getFormatPlugin()->defaultExtension();
    $locations = array_map(function ($menu) use ($path, $extension) {
      $location = [
        $path,
        '/',
        $menu,
        '.',
        $extension,
      ];
      return implode('', $location);
    }, $menus);
    $description[] = $this->formatPlural(
      count($locations),
      'Your menu will be imported from:',
      'Your menus will be imported from:',
    );
    foreach ($locations as $location) {
      $description[] = $location;
    }
    return $description;
  }

  /**
   * {@inheritdoc}
   */
  public function configurationSummary() {
    return [
      $this->configuration['import_path'],
    ];
  }

  /**
   * Gets the source directory for the import source.
   *
   * @return string
   *   Returns the full path to the source directory.
   */
  protected function getSourceDirectory() {
    $path = [
      DRUPAL_ROOT,
      $this->configuration['import_path'],
    ];
    return implode('/', $path);
  }

}
