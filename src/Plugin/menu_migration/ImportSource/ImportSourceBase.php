<?php

namespace Drupal\menu_migration\Plugin\menu_migration\ImportSource;

use Drupal\menu_migration\MenuMigrationException;
use Drupal\menu_migration\Plugin\FormatManager;
use Drupal\menu_migration\Plugin\ImportExportPluginBase;
use Drupal\menu_migration\Plugin\ImportSourceInterface;
use Drupal\menu_migration\Plugin\ImportSourceManager;
use Drupal\menu_migration\Service\MenuMigrationService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a base implementation for an ImportSource plugin.
 *
 * @see \Drupal\menu_migration\Annotation\MenuMigrationSource
 * @see \Drupal\menu_migration\Plugin\ImportSourceManager
 * @see \Drupal\menu_migration\Plugin\ImportSourceInterface
 * @see plugin_api
 */
abstract class ImportSourceBase extends ImportExportPluginBase implements ImportSourceInterface {

  /**
   * The import source plugin manager.
   *
   * @var \Drupal\menu_migration\Plugin\ImportSourceManager
   */
  protected ImportSourceManager $sourceManager;

  public function __construct(array $configuration, $plugin_id, $plugin_definition, FormatManager $formatManager, MenuMigrationService $menuMigrationService, ImportSourceManager $sourceManager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $formatManager, $menuMigrationService);
    $this->sourceManager = $sourceManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.menu_migration_format'),
      $container->get('menu_migration.import_export'),
      $container->get('plugin.manager.menu_migration_source'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function import() {
    $result = [];
    $menus = $this->getMenus();
    foreach ($menus as $menuName) {
      // At the moment, we enforce the deletion of existing items.
      // @todo Ideally, this should be optional, but I still need time to see how we can manage this.
      $this->menuMigrationService->deleteExistingMenuItems($menuName);
      try {
        $importStatus = $this->importMenu($menuName);
        if ($importStatus) {
          $result['success'][] = $menuName;
        }
        else {
          $result['failure'][] = $menuName;
        }
      }
      catch (MenuMigrationException $e) {
        $result['failure'][] = $menuName;
        $result['errors'][] = $e->getMessage();
      }
    }
    if (!empty($result['errors'])) {
      // Remove duplicate error messages.
      $result['errors'] = array_unique($result['errors']);
    }
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function getImportDescription() {
    $description[] = $this->formatPlural(
      count($this->getMenus()),
      'The following menu: %menus will be imported from the %source source using the %format format.',
      'The following menus: %menus will be imported from the %source source using the %format format.',
      [
        '%menus' => implode(', ', $this->getMenus()),
        '%source' => $this->label(),
        '%format' => $this->getFormatPlugin()->label(),
      ],
    );
    $description[] = $this->t('All existing menu items will be <strong>deleted</strong> prior to the import.');
    return $description;
  }

}
