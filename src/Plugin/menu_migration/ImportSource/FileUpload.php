<?php

namespace Drupal\menu_migration\Plugin\menu_migration\ImportSource;

use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\file\FileRepositoryInterface;
use Drupal\menu_migration\Attribute\MenuMigrationSource;
use Drupal\menu_migration\MenuMigrationException;
use Drupal\menu_migration\Plugin\FormatManager;
use Drupal\menu_migration\Plugin\ImportExportActionPluginInterface;
use Drupal\menu_migration\Plugin\ImportSourceManager;
use Drupal\menu_migration\Service\MenuMigrationService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a file upload import source.
 */
#[MenuMigrationSource(
  id: 'file_upload',
  label: new TranslatableMarkup('File Upload'),
  multiple: FALSE
)]
class FileUpload extends ImportSourceBase implements ImportExportActionPluginInterface {

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected FileSystemInterface $fileSystem;

  /**
   * The File Repository service.
   *
   * @var \Drupal\file\FileRepositoryInterface
   */
  protected FileRepositoryInterface $fileRepository;

  /**
   * The uploaded file URI used for import.
   *
   * @var string
   */
  protected string $fileUri;

  public function __construct(array $configuration, $plugin_id, $plugin_definition, FormatManager $formatManager, MenuMigrationService $menuMigrationService, ImportSourceManager $sourceManager, FileSystemInterface $fileSystem, FileRepositoryInterface $fileRepository) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $formatManager, $menuMigrationService, $sourceManager);
    $this->fileSystem = $fileSystem;
    $this->fileRepository = $fileRepository;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.menu_migration_format'),
      $container->get('menu_migration.import_export'),
      $container->get('plugin.manager.menu_migration_source'),
      $container->get('file_system'),
      $container->get('file.repository'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildActionForm(array $form, FormStateInterface $form_state) {
    $format = $this->getFormatPlugin();
    $form['source_file'] = [
      '#type' => 'file',
      '#title' => $this->t('File'),
      '#description' => $this->t('Upload a file containing the menu items to import. Allowed extensions: %extensions.', [
        '%extensions' => implode(', ', $format->allowedExtensions()),
      ]),
      '#required' => TRUE,
      '#upload_validators' => [
        'FileExtension' => [
          'extensions' => implode(' ', $format->allowedExtensions()),
        ],
      ],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateActionForm(array $form, FormStateInterface $form_state) {
    $format = $this->getFormatPlugin();
    $validators = [
      'FileExtension' => [
        'extensions' => implode(' ', $format->allowedExtensions()),
      ],
    ];
    // Unfortunately, file_save_upload does not work properly with subforms.
    // Because of this, we are using the container name instead of the file
    // field name as a workaround.
    $sourceFile = file_save_upload('action_form', $validators, FALSE, 0);
    if ($sourceFile) {
      $this->fileUri = $sourceFile->getFileUri();
    }
    else {
      // We need to manually trigger an empty error because of the above
      // workaround, otherwise the validation will pass.
      $form_state->setErrorByName('source_file');
    }

  }

  /**
   * {@inheritdoc}
   */
  public function submitActionForm(array $form, FormStateInterface $form_state) {}

  /**
   * {@inheritdoc}
   */
  public function importMenu(string $menuName) {
    if (!empty($this->fileUri)) {
      $contents = file_get_contents($this->fileUri);
      $items = $this->getFormatPlugin()->decode($contents);
      try {
        $this->menuMigrationService->generateMenuItems($items, $menuName);
        return TRUE;
      }
      catch (EntityStorageException $e) {
        throw new MenuMigrationException($e->getMessage());
      }
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getImportDescription() {
    $description = parent::getImportDescription();
    $description[] = $this->t('The selected menu will be imported from the uploaded file.');
    return $description;
  }

}
