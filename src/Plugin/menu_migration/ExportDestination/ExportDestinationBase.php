<?php

namespace Drupal\menu_migration\Plugin\menu_migration\ExportDestination;

use Drupal\menu_migration\MenuMigrationException;
use Drupal\menu_migration\Plugin\ExportDestinationInterface;
use Drupal\menu_migration\Plugin\ExportDestinationManager;
use Drupal\menu_migration\Plugin\FormatManager;
use Drupal\menu_migration\Plugin\ImportExportPluginBase;
use Drupal\menu_migration\Service\MenuMigrationService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a base implementation for an ExportDestination plugin.
 *
 * @see \Drupal\menu_migration\Annotation\MenuMigrationDestination
 * @see \Drupal\menu_migration\Plugin\ExportDestinationManager
 * @see \Drupal\menu_migration\Plugin\ExportDestinationInterface
 * @see plugin_api
 */
abstract class ExportDestinationBase extends ImportExportPluginBase implements ExportDestinationInterface {

  /**
   * The export destination plugin manager.
   *
   * @var \Drupal\menu_migration\Plugin\ExportDestinationManager
   */
  protected ExportDestinationManager $destinationManager;

  public function __construct(array $configuration, $plugin_id, $plugin_definition, FormatManager $formatManager, MenuMigrationService $menuMigrationService, ExportDestinationManager $destinationManager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $formatManager, $menuMigrationService);
    $this->destinationManager = $destinationManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.menu_migration_format'),
      $container->get('menu_migration.import_export'),
      $container->get('plugin.manager.menu_migration_destination'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function export() {
    $result = [];
    $menus = $this->getMenus();
    foreach ($menus as $menuName) {
      try {
        $exportStatus = $this->exportMenu($menuName);
        if ($exportStatus) {
          $result['success'][] = $menuName;
        }
        else {
          $result['failure'][] = $menuName;
        }
      }
      catch (MenuMigrationException $e) {
        $result['failure'][] = $menuName;
        $result['errors'][] = $e->getMessage();
      }
    }
    if (!empty($result['errors'])) {
      // Remove duplicate error messages.
      $result['errors'] = array_unique($result['errors']);
    }
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function getExportDescription() {
    $description[] = $this->formatPlural(
      count($this->getMenus()),
      'The following menu: %menus will be exported to the %destination destination using the %format format.',
      'The following menus: %menus will be exported to the %destination destination using the %format format.',
      [
        '%menus' => implode(', ', $this->getMenus()),
        '%destination' => $this->label(),
        '%format' => $this->getFormatPlugin()->label(),
      ],
    );
    return $description;
  }

}
