<?php

namespace Drupal\menu_migration\Plugin\menu_migration\ExportDestination;

use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\menu_migration\Attribute\MenuMigrationDestination;
use Drupal\menu_migration\MenuMigrationException;
use Drupal\menu_migration\Plugin\ExportDestinationManager;
use Drupal\menu_migration\Plugin\FormatManager;
use Drupal\menu_migration\Service\MenuMigrationService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a codebase export destination.
 */
#[MenuMigrationDestination(
  id: 'codebase',
  label: new TranslatableMarkup('Codebase'),
  cli: TRUE
)]
class Codebase extends ExportDestinationBase {

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected FileSystemInterface $fileSystem;

  public function __construct(array $configuration, $plugin_id, $plugin_definition, FormatManager $formatManager, MenuMigrationService $menuMigrationService, ExportDestinationManager $destinationManager, FileSystemInterface $fileSystem) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $formatManager, $menuMigrationService, $destinationManager);
    $this->fileSystem = $fileSystem;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.menu_migration_format'),
      $container->get('menu_migration.import_export'),
      $container->get('plugin.manager.menu_migration_destination'),
      $container->get('file_system'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'export_path' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function exportMenu(string $menuName) {
    $menuTree = $this->menuMigrationService->getMenuTree($menuName);
    $directory = $this->getTargetDirectory();
    if (!$this->fileSystem->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS)) {
      throw new MenuMigrationException(sprintf('Failed to create the "%s" directory, or the directory exists and it is not writeable.', $directory));
    }
    $data = $this->getFormatPlugin()->encode($menuTree);
    $extension = $this->getFormatPlugin()->defaultExtension();

    $file = "{$directory}/{$menuName}.{$extension}";
    $bytes = @file_put_contents($file, $data);
    if ($bytes === FALSE) {
      throw new MenuMigrationException(sprintf('Could not write to the "%s" file.', $file));
    }
    return !empty($bytes);
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['export_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Export directory path'),
      '#description' => $this->t("Enter the path where the exported items should be stored (relative to Drupal root - %root_path), no trailing slash. <br>Your menu(s) will be stored in this exact location. <br>E.g. If the defined path is %export_path, the selected menus will be exported to %export_location.", [
        '%root_path' => DRUPAL_ROOT,
        '%export_path' => !empty($this->configuration['export_path']) ? $this->configuration['export_path'] : '../config/menu_migration/DIRECTORY',
        '%export_location' => !empty($this->configuration['export_path']) ? $this->getTargetDirectory() : $this->getTargetDirectory() . '../config/menu_migration/DIRECTORY',
      ]),
      '#default_value' => $this->configuration['export_path'],
      '#attributes' => ['placeholder' => '../config/menu_migration/DIRECTORY'],
      '#required' => TRUE,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);
    // Ensure that there are no trailing slashes, nor white spaces.
    $form_state->setValue('export_path', trim(rtrim($form_state->getValue('export_path'), '/')));
    $exportPath = $form_state->getValue('export_path');

    if (!empty($exportPath)) {
      $filename = DRUPAL_ROOT . '/' . $exportPath;
      if (!$this->fileSystem->prepareDirectory($filename, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS)) {
        $form_state->setErrorByName('export_path', $this->t('Could not prepare destination directory @dir for menu migrations.', [
          '@dir' => $filename,
        ]));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getExportDescription() {
    $description = parent::getExportDescription();
    $menus = $this->getMenus();
    $path = $this->configuration['export_path'];
    $extension = $this->getFormatPlugin()->defaultExtension();
    $locations = array_map(function ($menu) use ($path, $extension) {
      $location = [
        $path,
        '/',
        $menu,
        '.',
        $extension,
      ];
      return implode('', $location);
    }, $menus);
    $description[] = $this->formatPlural(
      count($locations),
      'Your menu will be exported to:',
      'Your menus will be exported to:',
    );
    foreach ($locations as $location) {
      $description[] = $location;
    }
    return $description;
  }

  /**
   * {@inheritdoc}
   */
  public function configurationSummary() {
    return [
      $this->configuration['export_path'],
    ];
  }

  /**
   * Gets the target directory for the export destination.
   *
   * @return string
   *   Returns the full path to the target directory.
   */
  protected function getTargetDirectory() {
    $path = [
      DRUPAL_ROOT,
      $this->configuration['export_path'],
    ];
    return implode('/', $path);
  }

}
