<?php

namespace Drupal\menu_migration\Plugin\menu_migration\ExportDestination;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\menu_migration\Attribute\MenuMigrationDestination;
use Symfony\Component\HttpFoundation\Response;

/**
 * Provides a download export destination.
 */
#[MenuMigrationDestination(
  id: 'download',
  label: new TranslatableMarkup('Download'),
  multiple: FALSE
)]
class Download extends ExportDestinationBase {

  /**
   * {@inheritdoc}
   */
  public function exportMenu(string $menuName) {
    $menuTree = $this->menuMigrationService->getMenuTree($menuName);
    $data = $this->getFormatPlugin()->encode($menuTree);
    $fileName = $menuName . '.' . $this->formatPlugin->defaultExtension();
    $response = new Response($data);
    $response->headers->set('Content-Type', $this->formatPlugin->mimeType());
    $response->headers->set('Content-Disposition', 'attachment; filename="' . $fileName . '"');
    // Unfortunately, once we initiate the download, the rest of the code won't
    // be reached anymore.
    // @todo If possible, find a way to improve this without returning the response object.
    $response->send();
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getExportDescription() {
    $description = parent::getExportDescription();
    $configuration = $this->getConfiguration();
    /** @var \Drupal\menu_migration\Entity\ExportTypeInterface $exportType */
    $exportType = $configuration['#export_type'];
    // As a workaround for the problem described in exportMenu, we inform the
    // user to click on the collection link when they finish the download.
    // @todo This should be adjusted if a real solution is found for the original problem
    $description[] = $this->t('The selected menu will be exported as a downloadable file.');
    $description[] = $this->t('Click <a href=":url">here</a> after you finish downloading the file.', [
      ':url' => $exportType->toUrl('collection')->toString(),
    ]);
    return $description;
  }

}
