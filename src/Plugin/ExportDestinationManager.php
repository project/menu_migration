<?php

namespace Drupal\menu_migration\Plugin;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\menu_migration\Attribute\MenuMigrationDestination;

/**
 * Provides an Export Destination plugin manager.
 *
 * @see \Drupal\menu_migration\Attribute\MenuMigrationDestination
 * @see \Drupal\menu_migration\Annotation\MenuMigrationDestination
 * @see \Drupal\menu_migration\Plugin\ExportDestinationInterface
 * @see \Drupal\menu_migration\Plugin\menu_migration\ExportDestination\ExportDestinationBase
 * @see plugin_api
 */
class ExportDestinationManager extends DefaultPluginManager {

  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/menu_migration/ExportDestination',
      $namespaces,
      $module_handler,
      'Drupal\menu_migration\Plugin\ExportDestinationInterface',
      MenuMigrationDestination::class,
      'Drupal\menu_migration\Annotation\MenuMigrationDestination',
    );
    $this->alterInfo('menu_migration_destination_info');
    $this->setCacheBackend($cache_backend, 'menu_migration_destination_info');
  }

  /**
   * Gets the available export destinations as associative array.
   *
   * @return array
   *   Returns an associative array of the export destinations keyed by their
   *   ID, having their label as the value.
   */
  public function getExportDestinations() {
    return array_map(function ($definition) {
      return $definition['label'];
    }, $this->getDefinitions());
  }

}
