<?php

namespace Drupal\menu_migration\Plugin;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Core\Plugin\PluginFormInterface;

/**
 * Common interface for source and destination plugins.
 */
interface ImportExportPluginInterface extends PluginFormInterface, ConfigurableInterface {

  /**
   * Gets the label of the Plugin.
   *
   * @return string
   *   The plugin label.
   */
  public function label();

  /**
   * Determines if the plugin can import more than one menu at a time.
   *
   * @return bool
   *   Returns TRUE if the plugin can import more than one menu at a time, FALSE
   *   otherwise.
   */
  public function handleMultiple();

  /**
   * Determines if the plugin can handle Drush.
   *
   * @return bool
   *   Returns TRUE if the plugin can be used with Drush, FALSE otherwise.
   */
  public function handleCli();

  /**
   * Gets the selected menus of a source/destination plugin.
   *
   * @return array
   *   Returns on associative array of the menus, where the value is the machine
   *   name of the menu. The key can either be an integer, either the menu name
   *   depending on if checkboxes or radios were used in the configuration form.
   */
  public function getMenus();

  /**
   * Gets the plugin ID of the selected format.
   *
   * @return string|null
   *   The ID of the format plugin ID if set, NULL otherwise.
   */
  public function getFormat();

  /**
   * Gets the Format Plugin, if valid.
   *
   * @return \Drupal\menu_migration\Plugin\FormatInterface|null
   *   Returns the format plugin instance if valid, NULL otherwise.
   */
  public function getFormatPlugin();

  /**
   * Determines if the selected format is valid or exists.
   *
   * @return bool
   *   Returns TRUE if there is a valid format ID set, FALSE otherwise.
   */
  public function hasValidFormat();

  /**
   * Gets the summary of the source/destination configuration.
   *
   * This summary is used for the UI listings at the moment to display
   * additional information about the selected configured source or destination
   * plugin.
   *
   * @return array
   *   An array of strings, each value representing a line.
   */
  public function configurationSummary();

}
