<?php

namespace Drupal\menu_migration\EventSubscriber;

use Drupal\Core\Site\Settings;
use Drupal\menu_migration\Event\MenuImportEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Alters menu items before they are imported.
 */
class ImportSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[MenuImportEvent::IMPORT_MENU_ITEM][] = ['onMenuItemImport'];
    return $events;
  }

  /**
   * Acts on a menu item before it's imported.
   *
   * @param \Drupal\menu_migration\Event\MenuImportEvent $event
   *   The MenuImportEvent.
   */
  public function onMenuItemImport(MenuImportEvent $event) {
    $replacements = Settings::get('menu_migration_replacements', []);
    foreach ($replacements as $rules) {
      $value = $event->getNestedValue($rules['parents']);
      $tokenKey = array_search(MenuImportEvent::IMPORT_MENU_SUBJECT_TOKEN, $rules['arguments']);
      if ($tokenKey) {
        $rules['arguments'][$tokenKey] = $value;
      }
      $newValue = call_user_func_array($rules['method'], $rules['arguments']);
      if ($newValue != $value) {
        // First one found, skip the rest of the rules.
        $event->setNestedValue($rules['parents'], $newValue);
        break;
      }
    }
  }

}
