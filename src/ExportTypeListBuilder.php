<?php

namespace Drupal\menu_migration;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Entity\EntityInterface;
use Drupal\menu_migration\Entity\ExportTypeInterface;
use Drupal\menu_migration\Plugin\ImportExportPluginInterface;

/**
 * Builds a listing of export type entities.
 */
class ExportTypeListBuilder extends ImportExportListBuilderBase {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('ID');
    $header['label'] = $this->t('Name');
    $header['menus'] = $this->t('Menus');
    $header['format'] = $this->t('Format');
    $header['destination'] = $this->t('Destination');
    $header['command'] = $this->t('Drush command');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface|ExportTypeInterface $entity) {
    $destination = $entity->getDestinationPlugin();
    $row['id'] = $entity->id();
    $row['label'] = $entity->label();
    $row['menus'] = implode(', ', $entity->getMenus());
    $row['format'] = $destination && $destination->getFormatPlugin() ? $destination->getFormatPlugin()->label() : '';
    $row['destination'] = '';
    if ($destination instanceof ImportExportPluginInterface) {
      $destinationInfo = array_merge([$destination->label()], $destination->configurationSummary());
      $row['destination'] = new FormattableMarkup(implode('<br>', $destinationInfo), []);
    }
    $row['command'] = $this->getFormattedEntityCommand($entity);
    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultOperations(EntityInterface $entity) {
    $export = [
      'title' => $this->t('Export'),
      'weight' => -50,
      'url' => $entity->toUrl('export-form'),
    ];

    return ['export' => $export] + parent::getDefaultOperations($entity);
  }

}
