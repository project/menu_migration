<?php

namespace Drupal\menu_migration\Attribute;

use Drupal\Component\Plugin\Attribute\Plugin;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Defines a MenuMigrationSource attribute object.
 *
 * Plugin Namespace: Plugin\menu_migration\ImportSource.
 *
 * @see \Drupal\menu_migration\Plugin\ImportSourceInterface
 * @see \Drupal\menu_migration\Plugin\ImportSourceManager
 * @see \Drupal\menu_migration\Plugin\menu_migration\ImportSource\ImportSourceBase
 * @see plugin_api
 */
#[\Attribute(\Attribute::TARGET_CLASS)]
class MenuMigrationSource extends Plugin {

  /**
   * Constructs a MenuMigrationSource attribute.
   *
   * @param string $id
   *   The plugin ID.
   * @param \Drupal\Core\StringTranslation\TranslatableMarkup $label
   *   The label of the import source.
   * @param bool $multiple
   *   Boolean indicating if the source can handle multiple menus.
   * @param bool $cli
   *   Boolean indicating if the source supports Drush.
   */
  public function __construct(
    public readonly string $id,
    public readonly TranslatableMarkup $label,
    public readonly bool $multiple = TRUE,
    public readonly bool $cli = FALSE,
  ) {}

}
