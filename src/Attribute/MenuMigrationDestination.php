<?php

namespace Drupal\menu_migration\Attribute;

use Drupal\Component\Plugin\Attribute\Plugin;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Defines a MenuMigrationDestination attribute object.
 *
 * Plugin Namespace: Plugin\menu_migration\ExportDestination.
 *
 * @see \Drupal\menu_migration\Plugin\ExportDestinationInterface
 * @see \Drupal\menu_migration\Plugin\ExportDestinationManager
 * @see \Drupal\menu_migration\Plugin\menu_migration\ExportDestination\ExportDestinationBase
 * @see plugin_api
 */
#[\Attribute(\Attribute::TARGET_CLASS)]
class MenuMigrationDestination extends Plugin {

  /**
   * Constructs a MenuMigrationDestination attribute.
   *
   * @param string $id
   *   The plugin ID.
   * @param \Drupal\Core\StringTranslation\TranslatableMarkup $label
   *   The label of the export destination.
   * @param bool $multiple
   *   Boolean indicating if the destination can handle multiple menus.
   * @param bool $cli
   *   Boolean indicating if the destination supports Drush.
   */
  public function __construct(
    public readonly string $id,
    public readonly TranslatableMarkup $label,
    public readonly bool $multiple = TRUE,
    public readonly bool $cli = FALSE,
  ) {}

}
