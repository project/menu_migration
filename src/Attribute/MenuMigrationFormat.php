<?php

namespace Drupal\menu_migration\Attribute;

use Drupal\Component\Plugin\Attribute\Plugin;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Defines a MenuMigrationFormat attribute object.
 *
 * Plugin Namespace: Plugin\menu_migration\Format.
 *
 * @see \Drupal\menu_migration\Plugin\FormatInterface
 * @see \Drupal\menu_migration\Plugin\FormatManager
 * @see \Drupal\menu_migration\Plugin\menu_migration\Format\FormatBase
 * @see plugin_api
 */
#[\Attribute(\Attribute::TARGET_CLASS)]
class MenuMigrationFormat extends Plugin {

  /**
   * Constructs a MenuMigrationFormat attribute.
   *
   * @param string $id
   *   The plugin ID.
   * @param \Drupal\Core\StringTranslation\TranslatableMarkup $label
   *   The label of the format.
   */
  public function __construct(
    public readonly string $id,
    public readonly TranslatableMarkup $label,
  ) {}

}
