<?php

namespace Drupal\menu_migration\Annotation;

use Drupal\Component\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;

/**
 * Defines a MenuMigrationFormat annotation object.
 *
 * @ingroup plugin_api
 *
 * @Annotation
 */
class MenuMigrationFormat extends Plugin {

  /**
   * A unique identifier for the Format plugin.
   *
   * @var string
   */
  public string $id;

  /**
   * The human-readable name of the Format type.
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public Translation $label;

}
