<?php

namespace Drupal\menu_migration\Annotation;

use Drupal\Component\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;

/**
 * Defines a MenuMigrationDestination annotation object.
 *
 * @ingroup plugin_api
 *
 * @Annotation
 */
class MenuMigrationDestination extends Plugin {

  /**
   * A unique identifier for the ExportDestination plugin.
   *
   * @var string
   */
  public string $id;

  /**
   * The human-readable name of the ExportDestination type.
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public Translation $label;

  /**
   * Boolean indicating if the destination can handle multiple menus.
   *
   * @var bool
   */
  public bool $multiple = TRUE;

  /**
   * Boolean indicating if the destination supports Drush.
   *
   * @var bool
   */
  public bool $cli = FALSE;

}
