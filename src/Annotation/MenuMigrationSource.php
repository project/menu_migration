<?php

namespace Drupal\menu_migration\Annotation;

use Drupal\Component\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;

/**
 * Defines a MenuMigrationSource annotation object.
 *
 * @ingroup plugin_api
 *
 * @Annotation
 */
class MenuMigrationSource extends Plugin {

  /**
   * A unique identifier for the ImportSource plugin.
   *
   * @var string
   */
  public string $id;

  /**
   * The human-readable name of the ImportSource type.
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public Translation $label;

  /**
   * Boolean indicating if the source can handle multiple menus.
   *
   * @var bool
   */
  public bool $multiple = TRUE;

  /**
   * Boolean indicating if the source supports Drush.
   *
   * @var bool
   */
  public bool $cli = FALSE;

}
