<?php

namespace Drupal\menu_migration;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Entity\EntityInterface;
use Drupal\menu_migration\Entity\ImportTypeInterface;
use Drupal\menu_migration\Plugin\ImportExportPluginInterface;

/**
 * Defines a class to build a listing of import type entities.
 *
 * @see \Drupal\menu_migration\Entity\ImportType
 */
class ImportTypeListBuilder extends ImportExportListBuilderBase {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('ID');
    $header['label'] = $this->t('Name');
    $header['menus'] = $this->t('Menus');
    $header['format'] = $this->t('Format');
    $header['source'] = $this->t('Source');
    $header['command'] = $this->t('Drush command');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface|ImportTypeInterface $entity) {
    $source = $entity->getSourcePlugin();
    $row['id'] = $entity->id();
    $row['label'] = $entity->label();
    $row['menus'] = implode(', ', $entity->getMenus());
    $row['format'] = $source && $source->getFormatPlugin() ? $source->getFormatPlugin()->label() : '';
    $row['source'] = '';
    if ($source instanceof ImportExportPluginInterface) {
      $sourceInfo = array_merge([$source->label()], $source->configurationSummary());
      $row['source'] = new FormattableMarkup(implode('<br>', $sourceInfo), []);
    }
    $row['command'] = $this->getFormattedEntityCommand($entity);
    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultOperations(EntityInterface $entity) {
    $import = [
      'title' => $this->t('Import'),
      'weight' => -50,
      'url' => $this->ensureDestination($entity->toUrl('import-form')),
    ];

    return ['import' => $import] + parent::getDefaultOperations($entity);
  }

}
