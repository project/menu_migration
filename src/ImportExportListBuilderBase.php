<?php

namespace Drupal\menu_migration;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\menu_migration\Entity\ExportTypeInterface;
use Drupal\menu_migration\Entity\ImportTypeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base entity list builder for import type and export type entities.
 */
class ImportExportListBuilderBase extends ConfigEntityListBuilder {

  /**
   * The module extension list service.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  protected ModuleExtensionList $moduleExtensionList;

  /**
   * The file URL generator service.
   *
   * @var \Drupal\Core\File\FileUrlGeneratorInterface
   */
  protected FileUrlGeneratorInterface $fileUrlGenerator;

  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage, ModuleExtensionList $moduleExtensionList, FileUrlGeneratorInterface $fileUrlGenerator) {
    parent::__construct($entity_type, $storage);
    $this->moduleExtensionList = $moduleExtensionList;
    $this->fileUrlGenerator = $fileUrlGenerator;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('extension.list.module'),
      $container->get('file_url_generator')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $output = parent::render();
    $output['#attached']['library'][] = 'menu_migration/clipboard';
    return $output;
  }

  /**
   * Builds the Drush command syntax with copy icon.
   *
   * @param \Drupal\Core\Entity\EntityInterface|\Drupal\menu_migration\Entity\ExportTypeInterface|\Drupal\menu_migration\Entity\ImportTypeInterface $entity
   *   The Export Type or Import Type entity.
   *
   * @return \Drupal\Component\Render\FormattableMarkup|\Drupal\Core\StringTranslation\TranslatableMarkup|string
   *   The translatable markup of the drush command with a copy icon if the
   *   entity supports Drush, N/A otherwise.
   */
  protected function getFormattedEntityCommand(EntityInterface|ExportTypeInterface|ImportTypeInterface $entity) {
    $copyIcon = $this->moduleExtensionList->getPath('menu_migration') . '/assets/img/copy-black.svg';
    $copyIcon = $this->fileUrlGenerator->generateString($copyIcon);
    return $entity->handleCli() ? new FormattableMarkup('<span id="mm-command-@id">@command</span><button id="mm-copy-@id"><img src=":icon" alt="Copy to Clipboard" title="Copy to Clipboard"></button>', [
      '@id' => $entity->id(),
      '@command' => $entity->getCommand(),
      ':icon' => $copyIcon,
    ]) : $entity->getCommand();
  }

}
