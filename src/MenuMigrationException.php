<?php

namespace Drupal\menu_migration;

/**
 * Represents an exception that occurred in some part of the Menu Migration.
 */
class MenuMigrationException extends \Exception {}
