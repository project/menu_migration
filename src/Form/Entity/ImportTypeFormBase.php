<?php

namespace Drupal\menu_migration\Form\Entity;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;
use Drupal\menu_migration\Plugin\ImportSourceManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form base for creating and editing import types.
 */
class ImportTypeFormBase extends EntityForm {

  /**
   * The import type entity.
   *
   * @var \Drupal\menu_migration\Entity\ImportType
   */
  protected $entity;

  /**
   * The Import Source Plugin Manager.
   *
   * @var \Drupal\menu_migration\Plugin\ImportSourceManager
   */
  protected ImportSourceManager $sourceManager;

  /**
   * The import type storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected EntityStorageInterface $importTypeStorage;

  public function __construct(ImportSourceManager $sourceManager, EntityTypeManagerInterface $entityTypeManager) {
    $this->sourceManager = $sourceManager;
    $this->importTypeStorage = $entityTypeManager->getStorage('mm_import_type');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.menu_migration_source'),
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    // If the form is being rebuilt, rebuild the entity with the current form
    // values.
    if ($form_state->isRebuilding()) {
      $this->entity = $this->buildEntity($form, $form_state);
    }
    $entity = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Menu import name'),
      '#default_value' => $entity->label(),
      '#required' => TRUE,
    ];
    $form['name'] = [
      '#type' => 'machine_name',
      '#machine_name' => [
        'exists' => [$this->importTypeStorage, 'load'],
      ],
      '#default_value' => $entity->id(),
      '#required' => TRUE,
    ];

    $sources = $this->sourceManager->getImportSources();
    $form['source'] = [
      '#type' => 'select',
      '#title' => $this->t('Source'),
      '#description' => $this->t('Choose from where you wish to import the menus.'),
      '#options' => $sources,
      '#required' => TRUE,
      '#empty_value' => '',
      '#default_value' => $entity->getSource(),
      '#ajax' => [
        'callback' => [get_class($this), 'buildAjaxSourceConfigForm'],
        'wrapper' => 'menu-migration-source-config-form',
        'effect' => 'fade',
      ],
    ];
    $this->buildSourceSubform($form, $form_state);

    return parent::form($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    // Handle the cases where the source changes.
    if ($this->entity->getSource() != $form_state->get('source')) {
      $input = &$form_state->getUserInput();
      $input['source_config'] = [];
      // We need to reset the form value as well, otherwise the previous ones
      // will persist and will cause unexpected error.
      try {
        /** @var \Drupal\menu_migration\Plugin\ImportSourceInterface $newSource */
        $newSource = $this->sourceManager->createInstance($form_state->getValue('source'));
        $form_state->setValue('source_config', $newSource->getConfiguration());
      }
      catch (PluginException $e) {
        $form_state->setValue('source_config', []);
      }
      $form_state->setRebuild();
    }
    // Call the validation handler of the selected source plugin.
    if ($this->entity->hasValidSource()) {
      $source = $this->entity->getSourcePlugin();
      $source_form_state = SubformState::createForSubform($form['source_config'], $form, $form_state);
      $source->validateConfigurationForm($form['source_config'], $source_form_state);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Call the submit handler of the selected source plugin.
    if ($this->entity->hasValidSource()) {
      $source = $this->entity->getSourcePlugin();
      $source_form_state = SubformState::createForSubform($form['source_config'], $form, $form_state);
      $source->submitConfigurationForm($form['source_config'], $source_form_state);
    }
    parent::submitForm($form, $form_state);
  }

  /**
   * Builds the Source plugin configuration subform.
   *
   * @param array $form
   *   An associative array containing the initial structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the complete form.
   */
  protected function buildSourceSubform(&$form, FormStateInterface $form_state) {
    $form['source_config'] = [];
    if ($this->entity->hasValidSource()) {
      // We need to know when the source changes to rebuild.
      $form_state->set('source', $this->entity->getSource());
      $source = $this->entity->getSourcePlugin();
      // Attach the source plugin configuration form.
      $source_form_state = SubformState::createForSubform($form['source_config'], $form, $form_state);
      $form['source_config'] = $source->buildConfigurationForm($form['source_config'], $source_form_state);
    }
    $form['source_config'] += [
      '#type' => 'container',
    ];
    $form['source_config']['#attributes']['id'] = 'menu-migration-source-config-form';
    $form['source_config']['#tree'] = TRUE;
  }

  /**
   * Handles switching the selected source plugin.
   *
   * @param array $form
   *   An associative array containing the initial structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the complete form.
   *
   * @return array
   *   The part of the form to return as AJAX.
   */
  public static function buildAjaxSourceConfigForm(array $form, FormStateInterface $form_state) {
    return $form['source_config'];
  }

}
