<?php

namespace Drupal\menu_migration\Form\Entity;

use Drupal\Core\Entity\EntityDeleteForm;

/**
 * Provides a form for deleting export types.
 */
class ExportTypeDeleteForm extends EntityDeleteForm {}
