<?php

namespace Drupal\menu_migration\Form\Entity;

use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;
use Drupal\menu_migration\Plugin\ImportExportActionPluginInterface;

/**
 * Provides a form for exporting export types.
 */
class ExportTypeExportForm extends EntityConfirmFormBase {

  /**
   * The entity being used by this form.
   *
   * @var \Drupal\menu_migration\Entity\ExportType
   */
  protected $entity;

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to export %name?', ['%name' => $this->entity->label()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return implode('<br>', $this->entity->getDestinationPlugin()->getExportDescription());
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Export');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return $this->entity->toUrl('collection');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $form['action_form'] = [];
    if ($this->entity->hasValidDestination()) {
      $destination = $this->entity->getDestinationPlugin();
      if ($destination instanceof ImportExportActionPluginInterface) {
        // Attach the destination action plugin form.
        $action_form_state = SubformState::createForSubform($form['action_form'], $form, $form_state);
        $form['action_form'] = $destination->buildActionForm($form['action_form'], $action_form_state);
      }
    }
    $form['action_form'] += [
      '#type' => 'container',
    ];
    // Without setting the subform as a tree, the values are not being retrieved
    // in the sub form state.
    // @see https://www.drupal.org/project/drupal/issues/3053890
    $form['action_form']['#tree'] = TRUE;
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    if ($this->entity->hasValidDestination()) {
      $destination = $this->entity->getDestinationPlugin();
      if ($destination instanceof ImportExportActionPluginInterface) {
        // Attach the destination action plugin form.
        $action_form_state = SubformState::createForSubform($form['action_form'], $form, $form_state);
        $destination->validateActionForm($form['action_form'], $action_form_state);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if ($this->entity->hasValidDestination()) {
      $destination = $this->entity->getDestinationPlugin();
      if ($destination instanceof ImportExportActionPluginInterface) {
        // Attach the destination action plugin form.
        $action_form_state = SubformState::createForSubform($form['action_form'], $form, $form_state);
        $destination->submitActionForm($form['action_form'], $action_form_state);
      }
    }
    $result = $this->entity->export();
    if (!empty($result['success'])) {
      $this->messenger()->addStatus($this->t("Successfully exported the following menu(s): %menus", [
        '%menus' => implode(', ', $result['success']),
      ]));
    }
    if (!empty($result['failure'])) {
      $this->messenger()->addError($this->t("The following menus failed to be exported: %menus. Please check the logs.", [
        '%menus' => implode(', ', $result['failure']),
      ]));
    }
    if (!empty($result['errors'])) {
      $this->messenger()->addError(implode('<br>', $result['errors']));
    }
    $form_state->setRedirectUrl($this->getCancelUrl());
  }

}
