<?php

namespace Drupal\menu_migration\Form\Entity;

use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;
use Drupal\menu_migration\Plugin\ImportExportActionPluginInterface;

/**
 * Provides a form for importing import types.
 */
class ImportTypeImportForm extends EntityConfirmFormBase {

  /**
   * The entity being used by this form.
   *
   * @var \Drupal\menu_migration\Entity\ImportType
   */
  protected $entity;

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to import %name?', ['%name' => $this->entity->label()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return implode('<br>', $this->entity->getSourcePlugin()->getImportDescription());
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Import');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return $this->entity->toUrl('collection');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $form['action_form'] = [];
    if ($this->entity->hasValidSource()) {
      $source = $this->entity->getSourcePlugin();
      if ($source instanceof ImportExportActionPluginInterface) {
        // Attach the source action plugin form.
        $action_form_state = SubformState::createForSubform($form['action_form'], $form, $form_state);
        $form['action_form'] = $source->buildActionForm($form['action_form'], $action_form_state);
      }
    }
    $form['action_form'] += [
      '#type' => 'container',
    ];
    // Without setting the subform as a tree, the values are not being retrieved
    // in the sub form state.
    // @see https://www.drupal.org/project/drupal/issues/3053890
    $form['action_form']['#tree'] = TRUE;
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    if ($this->entity->hasValidSource()) {
      $source = $this->entity->getSourcePlugin();
      if ($source instanceof ImportExportActionPluginInterface) {
        // Attach the source action plugin form.
        $action_form_state = SubformState::createForSubform($form['action_form'], $form, $form_state);
        $source->validateActionForm($form['action_form'], $action_form_state);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if ($this->entity->hasValidSource()) {
      $source = $this->entity->getSourcePlugin();
      if ($source instanceof ImportExportActionPluginInterface) {
        // Attach the source action plugin form.
        $action_form_state = SubformState::createForSubform($form['action_form'], $form, $form_state);
        $source->submitActionForm($form['action_form'], $action_form_state);
      }
    }
    $result = $this->entity->import();
    if (!empty($result['success'])) {
      $this->messenger()->addStatus($this->t("Successfully imported the following menu(s): %menus", [
        '%menus' => implode(', ', $result['success']),
      ]));
    }
    if (!empty($result['failure'])) {
      $this->messenger()->addError($this->t("The following menus failed to be imported: %menus. Please check the logs.", [
        '%menus' => implode(', ', $result['failure']),
      ]));
    }
    if (!empty($result['errors'])) {
      $this->messenger()->addError(implode('<br>', $result['errors']));
    }
    $form_state->setRedirectUrl($this->getCancelUrl());
  }

}
