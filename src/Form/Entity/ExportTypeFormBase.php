<?php

namespace Drupal\menu_migration\Form\Entity;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;
use Drupal\menu_migration\Plugin\ExportDestinationManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form base for creating and editing export types.
 */
class ExportTypeFormBase extends EntityForm {

  /**
   * The export type entity.
   *
   * @var \Drupal\menu_migration\Entity\ExportType
   */
  protected $entity;

  /**
   * The menu migration destination manager.
   *
   * @var \Drupal\menu_migration\Plugin\ExportDestinationManager
   */
  protected ExportDestinationManager $destinationManager;

  /**
   * The export type storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected EntityStorageInterface $exportTypeStorage;

  public function __construct(ExportDestinationManager $destinationManager, EntityTypeManagerInterface $entityTypeManager) {
    $this->destinationManager = $destinationManager;
    $this->exportTypeStorage = $entityTypeManager->getStorage('mm_export_type');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.menu_migration_destination'),
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    // If the form is being rebuilt, rebuild the entity with the current form
    // values.
    if ($form_state->isRebuilding()) {
      $this->entity = $this->buildEntity($form, $form_state);
    }
    $entity = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Menu export name'),
      '#default_value' => $entity->label(),
      '#required' => TRUE,
    ];
    $form['name'] = [
      '#type' => 'machine_name',
      '#machine_name' => [
        'exists' => [$this->exportTypeStorage, 'load'],
      ],
      '#default_value' => $entity->id(),
      '#required' => TRUE,
    ];

    $destinations = $this->destinationManager->getExportDestinations();
    $form['destination'] = [
      '#type' => 'select',
      '#title' => $this->t('Export Destination'),
      '#description' => $this->t('Select where you want your menu items to be exported to.'),
      '#options' => $destinations,
      '#required' => TRUE,
      '#empty_value' => '',
      '#default_value' => $entity->getDestination(),
      '#ajax' => [
        'callback' => [get_class($this), 'buildAjaxDestinationConfigForm'],
        'wrapper' => 'menu-migration-destination-config-form',
        'effect' => 'fade',
      ],
    ];
    $this->buildDestinationSubform($form, $form_state);

    return parent::form($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    // Handle the cases where the destination changes.
    if ($this->entity->getDestination() != $form_state->get('destination')) {
      $input = &$form_state->getUserInput();
      $input['destination_config'] = [];
      // We need to reset the form value as well, otherwise the previous ones
      // will persist and will cause unexpected error.
      try {
        /** @var \Drupal\menu_migration\Plugin\ExportDestinationInterface $newDestination */
        $newDestination = $this->destinationManager->createInstance($form_state->getValue('destination'));
        $form_state->setValue('destination_config', $newDestination->getConfiguration());
      }
      catch (PluginException $e) {
        $form_state->setValue('destination_config', []);
      }
      $form_state->setRebuild();
    }
    // Call the validation handler of the selected destination plugin.
    if ($this->entity->hasValidDestination()) {
      $destination = $this->entity->getDestinationPlugin();
      $destination_form_state = SubformState::createForSubform($form['destination_config'], $form, $form_state);
      $destination->validateConfigurationForm($form['destination_config'], $destination_form_state);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Call the submit handler of the selected destination plugin.
    if ($this->entity->hasValidDestination()) {
      $destination = $this->entity->getDestinationPlugin();
      $destination_form_state = SubformState::createForSubform($form['destination_config'], $form, $form_state);
      $destination->submitConfigurationForm($form['destination_config'], $destination_form_state);
    }
    parent::submitForm($form, $form_state);
  }

  /**
   * Builds the Destination plugin configuration subform.
   *
   * @param array $form
   *   An associative array containing the initial structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the complete form.
   */
  protected function buildDestinationSubform(array &$form, FormStateInterface $form_state) {
    $form['destination_config'] = [];
    if ($this->entity->hasValidDestination()) {
      // We need to know when the destination changes to rebuild.
      $form_state->set('destination', $this->entity->getDestination());
      $destination = $this->entity->getDestinationPlugin();
      // Attach the destination plugin configuration form.
      $destination_form_state = SubformState::createForSubform($form['destination_config'], $form, $form_state);
      $form['destination_config'] = $destination->buildConfigurationForm($form['destination_config'], $destination_form_state);
    }
    $form['destination_config'] += [
      '#type' => 'container',
    ];
    $form['destination_config']['#attributes']['id'] = 'menu-migration-destination-config-form';
    $form['destination_config']['#tree'] = TRUE;
  }

  /**
   * Handles switching the selected destination plugin.
   *
   * @param array $form
   *   An associative array containing the initial structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the complete form.
   *
   * @return array
   *   The part of the form to return as AJAX.
   */
  public static function buildAjaxDestinationConfigForm(array $form, FormStateInterface $form_state) {
    return $form['destination_config'];
  }

}
