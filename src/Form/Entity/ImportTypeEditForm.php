<?php

namespace Drupal\menu_migration\Form\Entity;

use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a form for editing import types.
 */
class ImportTypeEditForm extends ImportTypeFormBase {

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $form_state->setRedirectUrl($this->entity->toUrl('collection'));
    $this->messenger()->addStatus($this->t('The menu import %name was successfully updated.', ['%name' => $this->entity->label()]));
  }

}
