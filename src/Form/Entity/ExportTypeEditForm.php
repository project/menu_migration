<?php

namespace Drupal\menu_migration\Form\Entity;

use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a form for editing export types.
 */
class ExportTypeEditForm extends ExportTypeFormBase {

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $form_state->setRedirectUrl($this->entity->toUrl('collection'));
    $this->messenger()->addStatus($this->t('The menu export %name was successfully updated.', ['%name' => $this->entity->label()]));
  }

}
