<?php

namespace Drupal\menu_migration\Form\Entity;

use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a form for creating import types.
 */
class ImportTypeAddForm extends ImportTypeFormBase {

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $form_state->setRedirectUrl($this->entity->toUrl('collection'));
    $this->messenger()->addStatus($this->t('The menu import %name was created.', ['%name' => $this->entity->label()]));
  }

  /**
   * {@inheritdoc}
   */
  public function actions(array $form, FormStateInterface $form_state) {
    $actions = parent::actions($form, $form_state);
    $actions['submit']['#value'] = $this->t('Create new menu import');

    return $actions;
  }

}
