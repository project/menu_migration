<?php

namespace Drupal\menu_migration\Form\Entity;

use Drupal\Core\Entity\EntityDeleteForm;

/**
 * Provides a form for deleting import types.
 */
class ImportTypeDeleteForm extends EntityDeleteForm {}
