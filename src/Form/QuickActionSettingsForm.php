<?php

namespace Drupal\menu_migration\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\menu_migration\Plugin\ExportDestinationInterface;
use Drupal\menu_migration\Plugin\ExportDestinationManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form to configure Quick Action settings.
 */
class QuickActionSettingsForm extends ConfigFormBase {

  const QUICK_EXPORT_SETTINGS = 'menu_migration.quick_export';

  /**
   * The Export Destination plugin.
   *
   * @var \Drupal\menu_migration\Plugin\ExportDestinationInterface
   */
  protected ExportDestinationInterface $destination;

  public function __construct(ConfigFactoryInterface $config_factory, TypedConfigManagerInterface $typedConfigManager, ExportDestinationManager $destinationManager) {
    parent::__construct($config_factory, $typedConfigManager);
    $this->destination = $destinationManager->createInstance('codebase');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('config.typed'),
      $container->get('plugin.manager.menu_migration_destination'),
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [self::QUICK_EXPORT_SETTINGS];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'menu_migration_quick_action_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $this->destination->setConfiguration($this->config(self::QUICK_EXPORT_SETTINGS)->getRawData());
    $form['help'] = [
      '#markup' => $this->t('This form controls the format and the export/import path for quick exports/imports using the <strong>drush menu_migration:quick-export</strong> (alias: <em>drush mmqe</em>) and the <strong>drush menu_migration:quick-import</strong> (alias: <em>drush mmqi</em>) commands, skipping the creation of Configuration Entities.'),
    ];
    $config_keys = ['format', 'export_path'];
    $form = array_merge($form, $this->destination->buildConfigurationForm($form, $form_state));
    $form['menus']['#access'] = FALSE;
    foreach ($config_keys as $config_key) {
      $form[$config_key]['#config_target'] = self::QUICK_EXPORT_SETTINGS . ':' . $config_key;
    }
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $this->destination->validateConfigurationForm($form, $form_state);
    parent::validateForm($form, $form_state);
  }

}
