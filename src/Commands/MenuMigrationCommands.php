<?php

namespace Drupal\menu_migration\Commands;

use Consolidation\AnnotatedCommand\AnnotationData;
use Consolidation\AnnotatedCommand\CommandData;
use Consolidation\AnnotatedCommand\Hooks\HookManager;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\menu_migration\Entity\ExportType;
use Drupal\menu_migration\Entity\ExportTypeInterface;
use Drupal\menu_migration\Entity\ImportType;
use Drupal\menu_migration\Entity\ImportTypeInterface;
use Drupal\menu_migration\Form\QuickActionSettingsForm;
use Drupal\menu_migration\Plugin\FormatManager;
use Drupal\menu_migration\Plugin\ImportExportPluginInterface;
use Drupal\menu_migration\Service\MenuMigrationService;
use Drush\Attributes as CLI;
use Drush\Commands\DrushCommands;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputOption;

/**
 * Drush commands for the menu migration operations.
 */
class MenuMigrationCommands extends DrushCommands {

  const MENU_EXPORT = 'menu_migration:export';
  const MENU_EXPORT_LIST = 'menu_migration:export-list';
  const MENU_QUICK_EXPORT = 'menu_migration:quick-export';
  const MENU_IMPORT = 'menu_migration:import';
  const MENU_IMPORT_LIST = 'menu_migration:import-list';
  const MENU_QUICK_IMPORT = 'menu_migration:quick-import';

  /**
   * The menu migration service.
   *
   * @var \Drupal\menu_migration\Service\MenuMigrationService
   */
  protected MenuMigrationService $menuMigration;

  /**
   * The Export Type entity storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected EntityStorageInterface $exportTypeStorage;

  /**
   * The Import Type entity storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected EntityStorageInterface $importTypeStorage;

  /**
   * The quick export configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $quickExportConfig;

  /**
   * The Format plugin manager.
   *
   * @var \Drupal\menu_migration\Plugin\FormatManager
   */
  protected FormatManager $formatManager;

  public function __construct(EntityTypeManagerInterface $entityTypeManager, MenuMigrationService $menuMigrationService, ConfigFactoryInterface $config_factory, FormatManager $formatManager) {
    parent::__construct();
    $this->menuMigration = $menuMigrationService;
    $this->exportTypeStorage = $entityTypeManager->getStorage('mm_export_type');
    $this->importTypeStorage = $entityTypeManager->getStorage('mm_import_type');
    $this->quickExportConfig = $config_factory->get(QuickActionSettingsForm::QUICK_EXPORT_SETTINGS);
    $this->formatManager = $formatManager;
  }

  /**
   * Exports one or more menus using the given menu export ID.
   */
  #[CLI\Command(name: self::MENU_EXPORT, aliases: ['mme'])]
  #[CLI\Argument(name: 'export_type', description: 'The machine name (ID) of the Menu Export.')]
  #[CLI\Usage(name: self::MENU_EXPORT . ' my_menu_export', description: 'Export the configured menu(s) in the Menu Export with my_menu_export ID.')]
  public function export(string $export_type = '') {
    if (empty($export_type)) {
      $this->io()->error(dt('You must specify the ID of the Menu Export. Use drush mmel to see the available IDs.'));
    }
    else {
      $exportType = $this->exportTypeStorage->load($export_type);
      if ($exportType instanceof ExportTypeInterface) {
        if (!$exportType->handleCli()) {
          $this->io()->error(dt('The "!arg" Menu Export does not support drush. Use drush mmel to see the supported IDs.', [
            '!arg' => $export_type,
          ]));
        }
        else {
          $this->handleExport($exportType);
        }
      }
      else {
        $this->io()->error(dt('The "!arg" Menu Export does not exist. Please enter a valid Menu Export.', ['!arg' => $export_type]));
      }
    }
  }

  /**
   * Exports one or more menus using the given menu IDs.
   *
   * The format and the target directory can be configured in the "Quick Action
   * Settings". The format can be overridden.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  #[CLI\Command(name: self::MENU_QUICK_EXPORT, aliases: ['mmqe'])]
  #[CLI\Argument(name: 'menu_names', description: 'The machine name(s) (ID) of the menu(s). Delimit multiple using commas.')]
  #[CLI\Usage(name: self::MENU_QUICK_EXPORT . ' main', description: 'Export one menu (in this case "main")')]
  #[CLI\Usage(name: self::MENU_QUICK_EXPORT . ' main,footer,some_name', description: 'Export three menus (in this case "main", "footer" and "some_name")')]
  #[CLI\Usage(name: self::MENU_QUICK_EXPORT . ' main --format=yaml', description: 'Export one menu in YAML format')]
  #[CLI\Usage(name: self::MENU_QUICK_EXPORT . ' footer,main --format=json', description: 'Export two menus in JSON format')]
  public function quickExport(string $menu_names = '') {
    $menus = explode(',', $menu_names);
    if (empty($menu_names) || empty($menus)) {
      $this->io()->error('You must specify at least one menu to export.');
    }
    else {
      $exportMenus = [];
      $availableMenus = $this->menuMigration->getAvailableMenus();
      foreach ($menus as $menu) {
        if (!isset($availableMenus[$menu])) {
          $this->io()->warning(dt('The "!arg" menu does not exist. Skipping.', ['!arg' => $menu]));
        }
        else {
          $exportMenus[] = $menu;
        }
      }
      if (!empty($exportMenus)) {
        $quickExport = ExportType::create([
          'label' => 'Quick Export',
          'name' => 'quick_export',
          'destination' => 'codebase',
          'destination_config' => [
            'format' => $this->input()->getOption('format'),
            'menus' => $exportMenus,
            'export_path' => $this->quickExportConfig->get('export_path'),
          ],
        ]);
        $this->handleExport($quickExport);
      }
    }
  }

  /**
   * Adds dynamic options to the quick export command.
   */
  #[CLI\Hook(type: HookManager::OPTION_HOOK, target: self::MENU_QUICK_EXPORT)]
  public function quickExportOptions(Command $command, AnnotationData $annotationData) {
    $this->addDynamicFormatOption($command, $annotationData);
  }

  /**
   * Adds the options of the quick export command.
   */
  #[CLI\Hook(type: HookManager::ARGUMENT_VALIDATOR, target: self::MENU_QUICK_EXPORT)]
  public function quickExportValidate(CommandData $commandData) {
    $this->validateFormatOption($commandData);
  }

  /**
   * Lists the available menu exports.
   *
   * If there aren't any menu exports available, or none of the existing ones
   * are compatible with Drush, an empty table is returned.
   */
  #[CLI\Command(name: self::MENU_EXPORT_LIST, aliases: ['mmel'])]
  #[CLI\Usage(name: self::MENU_EXPORT_LIST, description: 'List all the available menu exports that are compatible with Drush.')]
  public function exportList() {
    /** @var \Drupal\menu_migration\Entity\ExportTypeInterface[] $exportTypes */
    $exportTypes = $this->exportTypeStorage->loadMultiple();
    $headers = [
      dt('ID'),
      dt('Name'),
      dt('Menus'),
      dt('Format'),
      dt('Destination'),
      dt('Drush Command'),
    ];
    $types = [];
    foreach ($exportTypes as $exportType) {
      if ($exportType->handleCli()) {
        $destination = $exportType->getDestinationPlugin();
        $destinationInfo = [];
        if ($destination instanceof ImportExportPluginInterface) {
          $destinationInfo = array_merge([$destination->label()], $destination->configurationSummary());
        }
        $types[] = [
          $exportType->id(),
          $exportType->label(),
          implode(', ', $exportType->getMenus()),
          $destination && $destination->getFormatPlugin() ? $destination->getFormatPlugin()->label() : '',
          implode(PHP_EOL, $destinationInfo),
          $exportType->getCommand(),
        ];
      }
    }
    $this->io()->table($headers, $types);
  }

  /**
   * Imports one or more menus using the given menu import ID.
   */
  #[CLI\Command(name: self::MENU_IMPORT, aliases: ['mmi'])]
  #[CLI\Argument(name: 'import_type', description: 'The machine name (ID) of the Menu Import.')]
  #[CLI\Usage(name: self::MENU_IMPORT . ' my_menu_import', description: 'Import the configured menu(s) in the Menu Import with my_menu_import ID.')]
  public function import(string $import_type = '') {
    if (empty($import_type)) {
      $this->io()->error(dt('You must specify the ID of the Menu Import. Use drush mmil to see the available IDs.'));
    }
    else {
      $importType = $this->importTypeStorage->load($import_type);
      if ($importType instanceof ImportTypeInterface) {
        if (!$importType->handleCli()) {
          $this->io()->error(dt('The "!arg" Menu Import does not support drush. Use drush mmil to see the supported IDs.', [
            '!arg' => $import_type,
          ]));
        }
        else {
          $this->handleImport($importType);
        }
      }
      else {
        $this->io()->error(dt('The "!arg" Menu Import does not exist. Please enter a valid Menu Import.', ['!arg' => $import_type]));
      }
    }
  }

  /**
   * Imports one or more menus using the given menu IDs.
   *
   * The format and the source directory can be configured in the "Quick Action
   * Settings".
   */
  #[CLI\Command(name: self::MENU_QUICK_IMPORT, aliases: ['mmqi'])]
  #[CLI\Argument(name: 'menu_names', description: 'The machine name(s) (ID) of the menu(s). Delimit multiple using commas.')]
  #[CLI\Usage(name: self::MENU_QUICK_IMPORT . ' main', description: 'Import one menu (in this case "main")')]
  #[CLI\Usage(name: self::MENU_QUICK_IMPORT . ' main,footer,some_name', description: 'Import three menus (in this case "main", "footer" and "some_name")')]
  #[CLI\Usage(name: self::MENU_QUICK_IMPORT . ' main --format=yaml', description: 'Import one menu in YAML format')]
  #[CLI\Usage(name: self::MENU_QUICK_IMPORT . ' footer,main --format=json', description: 'Import two menus in JSON format')]
  public function quickImport(string $menu_names = '') {
    $menus = explode(',', $menu_names);
    if (empty($menu_names) || empty($menus)) {
      $this->io()->error('You must specify at least one menu to import.');
    }
    else {
      $importMenus = [];
      $availableMenus = $this->menuMigration->getAvailableMenus();
      foreach ($menus as $menu) {
        if (!isset($availableMenus[$menu])) {
          $this->io()->warning(dt('The "!arg" menu does not exist. Skipping.', ['!arg' => $menu]));
        }
        else {
          $importMenus[] = $menu;
        }
      }
      if (!empty($importMenus)) {
        $quickImport = ImportType::create([
          'label' => 'Quick Import',
          'name' => 'quick_import',
          'source' => 'codebase',
          'source_config' => [
            'format' => $this->input()->getOption('format'),
            'menus' => $importMenus,
            'import_path' => $this->quickExportConfig->get('export_path'),
          ],
        ]);
        $this->handleImport($quickImport);
      }
    }
  }

  /**
   * Adds dynamic options to the quick import command.
   */
  #[CLI\Hook(type: HookManager::OPTION_HOOK, target: self::MENU_QUICK_IMPORT)]
  public function quickImportOptions(Command $command, AnnotationData $annotationData) {
    $this->addDynamicFormatOption($command, $annotationData);
  }

  /**
   * Validates the options of the quick import command.
   */
  #[CLI\Hook(type: HookManager::ARGUMENT_VALIDATOR, target: self::MENU_QUICK_IMPORT)]
  public function quickImportValidate(CommandData $commandData) {
    $this->validateFormatOption($commandData);
  }

  /**
   * Lists the available menu imports.
   *
   * If there aren't any menu imports available, or none of the existing ones
   * are compatible with Drush, an empty table is returned.
   */
  #[CLI\Command(name: self::MENU_IMPORT_LIST, aliases: ['mmil'])]
  #[CLI\Usage(name: self::MENU_IMPORT_LIST, description: 'List all the available menu imports that are compatible with Drush.')]
  public function importList() {
    /** @var \Drupal\menu_migration\Entity\ImportTypeInterface[] $importTypes */
    $importTypes = $this->importTypeStorage->loadMultiple();
    $headers = [
      dt('ID'),
      dt('Name'),
      dt('Menus'),
      dt('Format'),
      dt('Source'),
      dt('Drush Command'),
    ];
    $types = [];
    foreach ($importTypes as $importType) {
      if ($importType->handleCli()) {
        $source = $importType->getSourcePlugin();
        $sourceInfo = [];
        if ($source instanceof ImportExportPluginInterface) {
          $sourceInfo = array_merge([$source->label()], $source->configurationSummary());
        }
        $types[] = [
          $importType->id(),
          $importType->label(),
          implode(', ', $importType->getMenus()),
          $source && $source->getFormatPlugin() ? $source->getFormatPlugin()->label() : '',
          implode(PHP_EOL, $sourceInfo),
          $importType->getCommand(),
        ];
      }
    }
    $this->io()->table($headers, $types);
  }

  /**
   * Exports the given Export Type and handles the response.
   *
   * @param \Drupal\menu_migration\Entity\ExportTypeInterface $exportType
   *   The Export Type object.
   */
  protected function handleExport(ExportTypeInterface $exportType) {
    $exportDescription = $exportType->getDestinationPlugin()->getExportDescription();
    foreach ($exportDescription as $description) {
      $this->io()->writeln(strip_tags($description));
    }
    if ($this->io()->confirm('Are you sure?')) {
      $result = $exportType->export();
      if (!empty($result['success'])) {
        $this->io()->success(dt('The following menu(s) were successfully exported: !menus', [
          '!menus' => implode(', ', $result['success']),
        ]));
      }
      if (!empty($result['failure'])) {
        $this->io()->error(dt('The following menu(s) failed to be exported: !menus', [
          '!menus' => implode(', ', $result['failure']),
        ]));
      }
      if (!empty($result['errors'])) {
        // Display additional error messages if any.
        $this->io()->error($result['errors']);
      }
    }
  }

  /**
   * Imports the given Import Type and handles the response.
   *
   * @param \Drupal\menu_migration\Entity\ImportTypeInterface $importType
   *   The Import Type object.
   */
  protected function handleImport(ImportTypeInterface $importType) {
    $importDescription = $importType->getSourcePlugin()->getImportDescription();
    foreach ($importDescription as $description) {
      $this->io()->writeln(strip_tags($description));
    }
    if ($this->io()->confirm('Are you sure?')) {
      $result = $importType->import();
      if (!empty($result['success'])) {
        $this->io()->success(dt('The following menu(s) were successfully imported: !menus', [
          '!menus' => implode(', ', $result['success']),
        ]));
      }
      if (!empty($result['failure'])) {
        $this->io()->error(dt('The following menu(s) failed to be imported: !menus', [
          '!menus' => implode(', ', $result['failure']),
        ]));
      }
      if (!empty($result['errors'])) {
        // Display additional error messages if any.
        $this->io()->error($result['errors']);
      }
    }
  }

  /**
   * Adds the dynamic format option to the quick export/import commands.
   *
   * @param \Symfony\Component\Console\Command\Command $command
   *   The command.
   * @param \Consolidation\AnnotatedCommand\AnnotationData $annotationData
   *   The annotation data.
   */
  protected function addDynamicFormatOption(Command $command, AnnotationData $annotationData) {
    $availableFormats = array_keys($this->formatManager->getFormats());
    $command->addOption(
      'format',
      '',
      InputOption::VALUE_OPTIONAL,
      dt('Set the format. The following values are supported: !valid_options.', [
        '!valid_options' => implode(', ', $availableFormats),
      ]),
      $this->quickExportConfig->get('format'),
    );
  }

  /**
   * Validates the format option for the quick export/import commands.
   *
   * @param \Consolidation\AnnotatedCommand\CommandData $commandData
   *   The command data.
   *
   * @throws \Exception
   */
  protected function validateFormatOption(CommandData $commandData) {
    $format = $commandData->input()->getOption('format');
    $availableFormats = array_keys($this->formatManager->getFormats());

    if (!in_array($format, $availableFormats)) {
      throw new \Exception(dt('Invalid format "!format". The format must have one of the following values: !valid_options.', [
        '!format' => $format,
        '!valid_options' => implode(', ', $availableFormats),
      ]));
    }
  }

}
