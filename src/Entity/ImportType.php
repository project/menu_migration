<?php

namespace Drupal\menu_migration\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\menu_migration\Plugin\ImportSourceInterface;

/**
 * Defines an import type configuration entity.
 *
 * @ConfigEntityType(
 *   id = "mm_import_type",
 *   label = @Translation("Menu import"),
 *   label_collection = @Translation("Menu imports"),
 *   label_singular = @Translation("menu import"),
 *   label_plural = @Translation("menu imports"),
 *   label_count = @PluralTranslation(
 *     singular = "@count menu import",
 *     plural = "@count menu imports",
 *   ),
 *   handlers = {
 *     "access" = "Drupal\menu_migration\Access\ImportExportAccessControlHandler",
 *     "form" = {
 *       "add" = "Drupal\menu_migration\Form\Entity\ImportTypeAddForm",
 *       "edit" = "Drupal\menu_migration\Form\Entity\ImportTypeEditForm",
 *       "delete" = "Drupal\menu_migration\Form\Entity\ImportTypeDeleteForm",
 *       "import" = "Drupal\menu_migration\Form\Entity\ImportTypeImportForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\menu_migration\ImportExportRouteProvider",
 *     },
 *     "list_builder" = "Drupal\menu_migration\ImportTypeListBuilder",
 *   },
 *   admin_permission = "administer menu migration import types",
 *   collection_permission = "perform import on menu migrations",
 *   entity_keys = {
 *     "id" = "name",
 *     "label" = "label"
 *   },
 *   links = {
 *     "import-form" = "/admin/config/development/menu-migration/import-types/{mm_import_type}/import",
 *     "edit-form" = "/admin/config/development/menu-migration/import-types/{mm_import_type}/edit",
 *     "delete-form" = "/admin/config/development/menu-migration/import-types/{mm_import_type}/delete",
 *     "add-form" = "/admin/config/development/menu-migration/import-types/add",
 *     "collection" = "/admin/config/development/menu-migration/import-types",
 *   },
 *   config_export = {
 *     "name",
 *     "label",
 *     "source",
 *     "source_config",
 *   }
 * )
 */
class ImportType extends ConfigEntityBase implements ImportTypeInterface {
  use StringTranslationTrait;

  /**
   * The displayed name of the Import Type.
   *
   * @var string
   */
  protected $name;

  /**
   * The ID of the Import Source plugin.
   *
   * @var string
   */
  protected $source;

  /**
   * The configuration of the destination plugin.
   *
   * @var array
   */
  protected array $source_config = [];

  /**
   * The Import Source plugin instance.
   *
   * @var \Drupal\menu_migration\Plugin\ImportSourceInterface|null
   */
  protected ?ImportSourceInterface $sourcePlugin;

  /**
   * {@inheritdoc}
   */
  public function id() {
    return $this->name;
  }

  /**
   * {@inheritdoc}
   */
  public function getSource() {
    return $this->source;
  }

  /**
   * {@inheritdoc}
   */
  public function getSourcePlugin() {
    if (!isset($this->sourcePlugin)) {
      $config = $this->source_config;
      $config['#import_type'] = $this;
      // @todo Need error handling or fallback plugin for this.
      $this->sourcePlugin = $this->importSourceManager()->createInstance($this->getSource(), $config);
    }
    return $this->sourcePlugin;
  }

  /**
   * {@inheritdoc}
   */
  public function hasValidSource() {
    $sourceDefinition = $this->importSourceManager()->getDefinition($this->getSource(), FALSE);
    return !empty($sourceDefinition);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormat() {
    if ($this->hasValidSource()) {
      return $this->getSourcePlugin()->getFormat();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getMenus() {
    if ($this->hasValidSource()) {
      return $this->getSourcePlugin()->getMenus();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function handleCli() {
    if ($this->hasValidSource()) {
      return $this->getSourcePlugin()->handleCli();
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getCommand() {
    return $this->handleCli() ? 'drush mmi ' . $this->id() : $this->t('N/A');
  }

  /**
   * {@inheritdoc}
   */
  public function import() {
    $source = $this->getSourcePlugin();
    return $source->import();
  }

  /**
   * Gets the import source plugin manager service.
   *
   * @return \Drupal\menu_migration\Plugin\ImportSourceManager
   *   Returns the Import Source plugin manager.
   */
  protected function importSourceManager() {
    return \Drupal::service('plugin.manager.menu_migration_source');
  }

  /**
   * Implements the magic __clone() method.
   *
   * Prevents the source plugin instance from being cloned, otherwise the entity
   * forms will have bad behaviour when changing sources.
   */
  public function __clone() {
    $this->sourcePlugin = NULL;
  }

}
