<?php

namespace Drupal\menu_migration\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\menu_migration\Plugin\ExportDestinationInterface;

/**
 * Defines an export type configuration entity.
 *
 * @ConfigEntityType(
 *   id = "mm_export_type",
 *   label = @Translation("Menu export"),
 *   label_collection = @Translation("Menu exports"),
 *   label_singular = @Translation("menu export"),
 *   label_plural = @Translation("menu exports"),
 *   label_count = @PluralTranslation(
 *     singular = "@count menu export",
 *     plural = "@count menu exports",
 *   ),
 *   handlers = {
 *     "access" = "Drupal\menu_migration\Access\ImportExportAccessControlHandler",
 *     "form" = {
 *       "add" = "Drupal\menu_migration\Form\Entity\ExportTypeAddForm",
 *       "edit" = "Drupal\menu_migration\Form\Entity\ExportTypeEditForm",
 *       "delete" = "Drupal\menu_migration\Form\Entity\ExportTypeDeleteForm",
 *       "export" = "Drupal\menu_migration\Form\Entity\ExportTypeExportForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\menu_migration\ImportExportRouteProvider",
 *     },
 *     "list_builder" = "Drupal\menu_migration\ExportTypeListBuilder",
 *   },
 *   admin_permission = "administer menu migration export types",
 *   collection_permission = "perform export on menu migrations",
 *   entity_keys = {
 *     "id" = "name",
 *     "label" = "label"
 *   },
 *   links = {
 *     "export-form" = "/admin/config/development/menu-migration/export-types/{mm_export_type}/export",
 *     "edit-form" = "/admin/config/development/menu-migration/export-types/{mm_export_type}/edit",
 *     "delete-form" = "/admin/config/development/menu-migration/export-types/{mm_export_type}/delete",
 *     "add-form" = "/admin/config/development/menu-migration/export-types/add",
 *     "collection" = "/admin/config/development/menu-migration/export-types",
 *   },
 *   config_export = {
 *     "name",
 *     "label",
 *     "destination",
 *     "destination_config",
 *   }
 * )
 */
class ExportType extends ConfigEntityBase implements ExportTypeInterface {
  use StringTranslationTrait;

  /**
   * The displayed name of the Export Type.
   *
   * @var string
   */
  protected $name;

  /**
   * The ID of the Export Destination plugin.
   *
   * @var string
   */
  protected $destination;

  /**
   * The configuration of the destination plugin.
   *
   * @var array
   */
  protected array $destination_config = [];

  /**
   * The Export Destination plugin instance.
   *
   * @var \Drupal\menu_migration\Plugin\ExportDestinationInterface|null
   */
  protected ?ExportDestinationInterface $destinationPlugin;

  /**
   * {@inheritdoc}
   */
  public function id() {
    return $this->name;
  }

  /**
   * {@inheritdoc}
   */
  public function getDestination() {
    return $this->destination;
  }

  /**
   * {@inheritdoc}
   */
  public function getDestinationPlugin() {
    if (!isset($this->destinationPlugin)) {
      $config = $this->destination_config;
      $config['#export_type'] = $this;
      // @todo Need error handling or fallback plugin for this.
      $this->destinationPlugin = $this->exportDestinationManager()->createInstance($this->getDestination(), $config);
    }
    return $this->destinationPlugin;
  }

  /**
   * {@inheritdoc}
   */
  public function hasValidDestination() {
    $destinationDefinition = $this->exportDestinationManager()->getDefinition($this->getDestination(), FALSE);
    return !empty($destinationDefinition);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormat() {
    if ($this->hasValidDestination()) {
      return $this->getDestinationPlugin()->getFormat();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getMenus() {
    if ($this->hasValidDestination()) {
      return $this->getDestinationPlugin()->getMenus();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function handleCli() {
    if ($this->hasValidDestination()) {
      return $this->getDestinationPlugin()->handleCli();
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getCommand() {
    return $this->handleCli() ? 'drush mme ' . $this->id() : $this->t('N/A');
  }

  /**
   * {@inheritdoc}
   */
  public function export() {
    $destination = $this->getDestinationPlugin();
    return $destination->export();
  }

  /**
   * Gets the export destination plugin manager service.
   *
   * @return \Drupal\menu_migration\Plugin\ExportDestinationManager
   *   Returns the Export Destination plugin manager.
   */
  protected function exportDestinationManager() {
    return \Drupal::service('plugin.manager.menu_migration_destination');
  }

  /**
   * Implements the magic __clone() method.
   *
   * Prevents the destination plugin instance from being cloned, otherwise the
   * entity forms will have bad behaviour when changing destinations.
   */
  public function __clone() {
    $this->destinationPlugin = NULL;
  }

}
