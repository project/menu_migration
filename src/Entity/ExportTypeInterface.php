<?php

namespace Drupal\menu_migration\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Interface for the Export Type configuration entity.
 */
interface ExportTypeInterface extends ConfigEntityInterface {

  /**
   * Gets the Export Destination plugin ID.
   *
   * @return string
   *   The ID of the Export Destination plugin.
   */
  public function getDestination();

  /**
   * Gets the Export Destination plugin instance.
   *
   * @return \Drupal\menu_migration\Plugin\ExportDestinationInterface
   *   The Export Destination plugin instance.
   */
  public function getDestinationPlugin();

  /**
   * Checks if the export destination is a valid plugin.
   *
   * @return bool
   *   Returns TRUE if the entity has a valid Export Destination plugin, FALSE
   *   otherwise.
   */
  public function hasValidDestination();

  /**
   * Gets the ID of the Format plugin.
   *
   * @return string
   *   The ID of the Format plugin.
   */
  public function getFormat();

  /**
   * Gets an array containing the menus.
   *
   * @return array
   *   An array containing the machine names of the selected menus. Keys can
   *   differ, but values are the same. When a destination plugin uses radios,
   *   the keys will be numeric.
   */
  public function getMenus();

  /**
   * Determines if the entity can handle CLI.
   *
   * @return bool
   *   Returns TRUE if the entity works with Drush, FALSE otherwise.
   */
  public function handleCli();

  /**
   * Gets the drush command if supported.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup|string
   *   A string containing the drush command if supported, N/A otherwise.
   */
  public function getCommand();

  /**
   * Executes the actual export of the selected menus.
   *
   * The structure of the response should match the example below. The keys are
   * not mandatory, meaning that if all menus succeeded, the failure key
   * doesn't need to be set.
   *
   * @code
   * $response = [
   *   'success' => $success_menus,
   *   'failure' => $failed_menus,
   *   'errors' => $additional_error_messages,
   * ];
   * @endcode
   *
   * @return array
   *   Returns an associative array with the menus that succeeded and the ones
   *   that failed. It can also contain additional error messages in the
   *   "errors" key for the menus that failed.
   */
  public function export();

}
