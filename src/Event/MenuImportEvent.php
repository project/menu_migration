<?php

namespace Drupal\menu_migration\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\Component\Utility\NestedArray;

/**
 * Event that is fired when a menu is being imported.
 */
class MenuImportEvent extends Event {

  const IMPORT_MENU_ITEM = 'menu_migration_import_item';
  const IMPORT_MENU_SUBJECT_TOKEN = '[SUBJECT]';

  /**
   * The menu link content item that is being imported.
   *
   * @var array
   */
  protected array $menuItem;

  /**
   * Constructs a new MenuImportEvent object.
   *
   * @param array $menuItem
   *   The menu link content item that is being imported.
   */
  public function __construct(array &$menuItem) {
    $this->menuItem = $menuItem;
  }

  /**
   * Gets the menu link content item that is being imported.
   *
   * @return array
   *   Returns the menu item array.
   */
  public function getMenuItem(): array {
    return $this->menuItem;
  }

  /**
   * Gets a nested value from the Menu Item.
   *
   * @param array $parents
   *   The parents of the value.
   *
   * @return array|mixed|null
   *   Returns the value if it exists.
   */
  public function getNestedValue(array $parents) {
    return NestedArray::getValue($this->menuItem, $parents);
  }

  /**
   * Sets the value of the given nested key in the current menu item array.
   *
   * @param array $parents
   *   The parents of the value to be set.
   * @param mixed $value
   *   The value to be set for the given parents.
   */
  public function setNestedValue(array $parents, mixed $value) {
    NestedArray::setValue($this->menuItem, $parents, $value);
  }

}
